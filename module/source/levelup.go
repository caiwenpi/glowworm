package source

import (
	"bytes"
	"glowworm/module/csvs"

	"gopkg.in/asaskevich/govalidator.v4"
)

// LevelUp 升级日志信息
type LevelUp struct {
	DwProduct
	DwIPTime
	DwAccount

	CurrentlyExp int32 `json:"currently_exp"`
	Exp          int32 `json:"exp"`
}

func NewLevelUp() ModelInterface {
	return new(LevelUp)
}

// Validate 验证数据
func (m *LevelUp) Validate() error {
	var err error
	_, err = govalidator.ValidateStruct(m)
	if err != nil {
		return err
	}

	err = m.processProduct()
	if err != nil {
		return err
	}
	err = m.processIPTime()
	if err != nil {
		return err
	}
	err = m.processAccount()
	if err != nil {
		return err
	}
	m.UniqueChrID = SetUniqueChrID(m.ProductID, m.PlatformID, m.GameserverID, m.ChrID)

	return nil
}

// Csv 准备上传数据
func (m *LevelUp) Csv() (string, error) {
	var s string
	msg := bytes.NewBufferString(s)
	sl := []*LevelUp{}
	sl = append(sl, m)
	if err := csvs.MarshalWithoutHeaders(&sl, msg); err != nil {
		return "", err
	}
	return msg.String(), nil
}

func init() {
	register("level-up", NewLevelUp)
}
