package setting

import (
	"fmt"
	"path/filepath"
	"time"

	"glowworm/model"
	"glowworm/module/hdfs"
	"glowworm/module/helper"
	"glowworm/module/httpapi"
	"glowworm/module/impala"
	"glowworm/module/kafka"
	"glowworm/module/mail"
	"glowworm/module/redis"

	"os"

	"github.com/Shopify/sarama"
	"github.com/Sirupsen/logrus"
	hdfsbase "github.com/colinmarc/hdfs"
	redisbase "github.com/go-redis/redis"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	"github.com/koblas/impalathing"
	"github.com/lestrrat/go-file-rotatelogs"
	"github.com/rifflock/lfshook"
	"github.com/spf13/viper"
	"github.com/urfave/cli"
)

const (
	TimestampFormat string = "2006-01-02 15:04:05"
	DateFormat      string = "2006-01-02"
)

var (
	cfg *viper.Viper = viper.New()

	Env string

	DataCategories []string
	RunInterval    time.Duration
	MaxErrCnt      int

	RootPath   string
	SourcePath string
	BackupPath string

	TablePassword string

	ParseGoroutineCount int
	SendBatchCount      int
)

func NewContext(c *cli.Context) {
	newConfig()
	newLog(c)

	Env = cfg.GetString("APP.ENV")
	// 生产环境添加邮件警报
	if Env == "pro" {
		NewMailHook()
	}

	DataCategories = cfg.GetStringSlice("APP.DATA_CATEGORY")
	logrus.Debug(DataCategories)
	RunInterval = cfg.GetDuration("APP.RUN_INTERVAL")
	MaxErrCnt = cfg.GetInt("APP.MAX_ERR_CNT")

	SourcePath = cfg.GetString("APP.SOURCE_PATH")
	BackupPath = cfg.GetString("APP.BACKUP_PATH")

	TablePassword = cfg.GetString("TABLE.PASSWORD")

	ParseGoroutineCount = cfg.GetInt("APP.PARSE_GOROUTINE_COUNT")
	SendBatchCount = cfg.GetInt("APP.SEND_BATCH_COUNT")
}

func newConfig() {
	var err error
	RootPath, err = helper.WorkDir()
	if err != nil {
		logrus.Error(err)
		return
	}

	cfg.SetConfigType("yaml")
	app := filepath.Join(RootPath, "conf/app.yaml")
	cfg.SetConfigFile(app)
	err = cfg.ReadInConfig()
	if err != nil {
		logrus.Error(err)
		return
	}

	local := filepath.Join(RootPath, "conf/local.yaml")
	if helper.IsFile(local) {
		cfg.SetConfigFile(local)
		err = cfg.MergeInConfig()
		if err != nil {
			logrus.Error(err)
			return
		}
	}

	for _, v := range cfg.AllSettings() {
		logrus.Info(v)
	}
}

func newLog(c *cli.Context) {
	dir := cfg.GetString("LOG.PATH")
	logrus.Infof("Log dir: %s", dir)
	action := os.Args[1]
	category := action + "-" + c.String("category")

	rl, err := rotatelogs.New(
		filepath.Join(dir, category+"-%Y-%m-%d.log"),
		rotatelogs.WithLinkName(cfg.GetString("LOG.LINK_NAME")+"-"+category),
		rotatelogs.WithMaxAge(cfg.GetDuration("LOG.MAX_DAYS")*24*time.Hour),
	)
	if err != nil {
		logrus.Error(err)
		return
	}

	hk := lfshook.NewHook(lfshook.WriterMap{
		logrus.InfoLevel:  rl,
		logrus.WarnLevel:  rl,
		logrus.ErrorLevel: rl,
	})
	/*
		hk.SetFormatter(&logrus.JSONFormatter{
			TimestampFormat: TimestampFormat,
		})
	*/
	logrus.AddHook(hk)

	logrus.SetFormatter(&logrus.TextFormatter{
		FullTimestamp:   true,
		ForceColors:     true,
		TimestampFormat: TimestampFormat,
	})

	lvl := logrus.Level(cfg.GetInt("LOG.LEVEL"))
	logrus.SetLevel(lvl)
}

func NewDBService() {
	user := cfg.GetString("MYSQL.USER")
	pass := cfg.GetString("MYSQL.PASSWORD")
	host := cfg.GetString("MYSQL.HOST")
	port := cfg.GetInt("MYSQL.PORT")
	database := cfg.GetString("MYSQL.DATABASE")

	var err error
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8&parseTime=True&loc=Local", user, pass, host, port, database)
	logrus.Debug(dsn)

	model.DB, err = gorm.Open("mysql", dsn)
	if err != nil {
		logrus.Error(err)
	}
}

func CloseDBService() {
	model.DB.Close()
}

func NewKafkaService() {
	addrs := cfg.GetStringSlice("KAFKA.ADDRS")
	var err error
	kafka.Producer, err = sarama.NewSyncProducer(addrs, nil)
	if err != nil {
		logrus.Errorf("Kafka new producer error: %v", err)
	}
}

func CloseKafkaService() {
	kafka.Producer.Close()
}

func NewImpalaService() {
	host := cfg.GetString("IMPALA.HOST")
	port := cfg.GetInt("IMPALA.PORT")
	database := cfg.GetString("IMPALA.DATABASE")

	var err error
	impala.Conn, err = impalathing.Connect(host, port, impalathing.DefaultOptions)
	if err != nil {
		logrus.Errorf("Impala connect error: %v", err)
	}

	q := "USE " + database
	err = impala.Query(q)
	if err != nil {
		logrus.Errorf("Use database %s error: %v", database, err)
	}
}

func CloseImpalaService() {
	impala.Conn.Close()
}

func NewHDFSService() {
	host := cfg.GetString("HDFS.HOST")
	port := cfg.GetInt("HDFS.PORT")

	hdfs.Path = cfg.GetString("HDFS.PATH")

	var err error
	hdfs.Client, err = hdfsbase.New(fmt.Sprintf("%s:%d", host, port))
	if err != nil {
		logrus.Errorf("HDFS client error: %v", err)
	}
}

func CloseHDFSService() {
	hdfs.Client.Close()
}

func NewHTTPApi() {
	httpapi.Url = cfg.GetString("HTTPAPI.URL")
	httpapi.Key = cfg.GetString("HTTPAPI.KEY")
}

func NewRedisService() {
	redis.Client = redisbase.NewClient(&redisbase.Options{
		Addr:     cfg.GetString("REDIS.ADDR"),
		Password: cfg.GetString("REDIS.PASSWORD"),
		DB:       cfg.GetInt("REDIS.DB"),
	})

	_, err := redis.Client.Ping().Result()
	if err != nil {
		logrus.Error(err)
	}
}

func NewMailHook() {
	subject := cfg.GetString("MAIL.SUBJECT")
	host := cfg.GetString("MAIL.HOST")
	port := cfg.GetInt("MAIL.PORT")
	from := cfg.GetString("MAIL.FROM")
	to := cfg.GetString("MAIL.TO")
	user := cfg.GetString("MAIL.USER")
	password := cfg.GetString("MAIL.PASSWORD")

	hk, err := mail.NewHook(subject, host, port, from, to, user, password)
	if err != nil {
		logrus.Error(err)
		return
	}
	logrus.AddHook(hk)
}
