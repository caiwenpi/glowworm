package hdfs

import (
	"os"

	"path"

	hdfsbase "github.com/colinmarc/hdfs"
)

var (
	Client *hdfsbase.Client
	Path   string
)

func ReadDir(dir string) ([]os.FileInfo, error) {
	p := path.Join(Path, dir)
	return Client.ReadDir(p)
}
