package table

import (
	"fmt"
	"glowworm/module/impala"
	"strings"

	"github.com/Sirupsen/logrus"
)

func virtualcurrency() {
	var err error
	table := "virtual_currencies"
	sl := []string{
		column(clmProduct),
		column(clmIPTime),
		column(clmAccount),
		`
		category_id TINYINT,
		category VARCHAR(64),
		project_id INT,
		project VARCHAR(64),
		info VARCHAR(255),
		op_type TINYINT,
		op_count INT,
		quantity_before BIGINT,
		quantity BIGINT
		`,
	}

	clms := strings.Join(sl, ",")

	raw := fmt.Sprintf(template(tmplRaw), table, clms, partition(pttPYM))

	err = impala.Query(raw)
	if err != nil {
		logrus.Error(err)
	}

	tmp := fmt.Sprintf(template(tmplTmp), table, clms, table)
	err = impala.Query(tmp)
	if err != nil {
		logrus.Error(err)
	}
}
