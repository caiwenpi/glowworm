package model

import (
	"encoding/json"
	"fmt"
	"glowworm/module/httpapi"
	"net/url"
	"sync"

	"github.com/Sirupsen/logrus"
)

var TaskMu = new(sync.Mutex)

// Task 任务
type Task struct {
	UniqueTaskID uint   `json:"unique_task_id" gorm:"column:unique_task_id;primary_key"`
	ProductID    uint   `json:"product_id" gorm:"column:product_id"`
	TaskID       int32  `json:"task_id" gorm:"column:task_id"`
	Name         string `json:"name" gorm:"column:name"`
}

// TableName 表名
func (Task) TableName() string {
	return "dw_tasks"
}

// GetTask 获取
func GetTask(pid uint, taskID int32, name string) (*Task, error) {
	m := newTask(pid, taskID, name)
	return m, GetModel(m)
}

func newTask(pid uint, taskID int32, name string) *Task {
	return &Task{
		ProductID: pid,
		TaskID:    taskID,
		Name:      name,
	}
}

func (m *Task) key() string {
	return fmt.Sprintf("task-%d-%d-%s", m.ProductID, m.TaskID, m.Name)
}

func (m *Task) Isset() bool {
	return m.UniqueTaskID != 0
}

func (m *Task) dbGet() {
	DB.Where(&Task{
		ProductID: m.ProductID,
		TaskID:    m.TaskID,
		Name:      m.Name,
	}).First(&m)
}

func (m *Task) remoteCreate() error {
	TaskMu.Lock()
	defer TaskMu.Unlock()

	body, err := httpapi.PostForm("task", url.Values{
		"product_id": {fmt.Sprint(m.ProductID)},
		"task_id":    {fmt.Sprint(m.TaskID)},
		"name":       {m.Name},
	})
	if err != nil {
		return err
	}

	tmp := new(Task)
	if err = json.Unmarshal(body, &tmp); err != nil {
		logrus.Warn(err)
	}
	m.UniqueTaskID = tmp.UniqueTaskID
	return nil
}

func (m *Task) errors() error {
	return fmt.Errorf("task not found product_id: %d, task_id: %d, name: %s", m.ProductID, m.TaskID, m.Name)
}
