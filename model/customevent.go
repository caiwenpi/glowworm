package model

import (
	"encoding/json"
	"fmt"
	"glowworm/module/httpapi"
	"net/url"
	"sync"

	"github.com/Sirupsen/logrus"
)

var CustomEventMu = new(sync.Mutex)

// CustomEvent 虚拟货币
type CustomEvent struct {
	EventID   uint   `json:"event_id" gorm:"column:event_id;primary_key"`
	ProductID uint   `json:"product_id" gorm:"column:product_id"`
	Name      string `json:"name" gorm:"column:name"`
}

// TableName 表名
func (CustomEvent) TableName() string {
	return "dw_custom_event"
}

// GetCustomEvent 获取
func GetCustomEvent(pid uint, name string) (*CustomEvent, error) {
	m := newCustomEvent(pid, name)
	return m, GetModel(m)
}

func newCustomEvent(pid uint, name string) *CustomEvent {
	return &CustomEvent{
		ProductID: pid,
		Name:      name,
	}
}

func (m *CustomEvent) key() string {
	return fmt.Sprintf("custom-event-%d-%s", m.ProductID, m.Name)
}

func (m *CustomEvent) Isset() bool {
	return m.EventID != 0
}

func (m *CustomEvent) dbGet() {
	DB.Where(&CustomEvent{
		ProductID: m.ProductID,
		Name:      m.Name,
	}).First(&m)
}

func (m *CustomEvent) remoteCreate() error {
	CustomEventMu.Lock()
	defer CustomEventMu.Unlock()

	body, err := httpapi.PostForm("custom-event", url.Values{
		"product_id": {fmt.Sprint(m.ProductID)},
		"name":       {m.Name},
	})
	if err != nil {
		return err
	}

	tmp := new(CustomEvent)
	if err = json.Unmarshal(body, &tmp); err != nil {
		logrus.Warn(err)
	}
	m.EventID = tmp.EventID
	return nil
}

func (m *CustomEvent) errors() error {
	return fmt.Errorf("custom event not found product_id: %d, name: %s", m.ProductID, m.Name)
}
