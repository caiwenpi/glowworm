package sink

import (
	"glowworm/module/impala"
)

type Login struct {
	Modeler
}

func newLogin() *Login {
	m := new(Login)
	m.dir = "login"
	m.table = "tmp_raw_logins"
	return m
}

func (m *Login) Load() error {
	var err error
	loginQuery := `
		INSERT INTO raw_logins PARTITION (pid, year, month)
		SELECT
			*,
			CAST(t.product_id AS TINYINT),
			CAST(FROM_UNIXTIME(t.time, 'yy') AS TINYINT),
			CAST(FROM_UNIXTIME(t.time, 'MM') AS TINYINT)
		FROM tmp_raw_logins AS t
	`
	err = impala.Query(loginQuery)
	if err != nil {
		return err
	}

	err = impala.Compute("raw_logins")
	if err != nil {
		return err
	}

	chrQuery := `
		INSERT INTO raw_chrs PARTITION (pid)
		SELECT
			t.product_id,
			CAST(MAX(t.product_name) AS VARCHAR(32)),
			CAST(MAX(t.platform_id) AS SMALLINT),
			CAST(MAX(t.platform_name) AS VARCHAR(32)),
			CAST(MAX(t.channel_id) AS SMALLINT),
			CAST(MAX(t.channel_name) AS VARCHAR(32)),
			CAST(MAX(t.gameserver_id) AS INT),
			CAST(MAX(t.gameserver_no) AS INT),
			CAST(MAX(t.ip) AS VARCHAR(15)),
			CAST(MAX(t.region) AS VARCHAR(128)),
			CAST(MAX(t.the_date) AS TIMESTAMP),
			CAST(MAX(t.the_date_time) AS TIMESTAMP),
			CAST(MAX(t.hour) AS TINYINT),
			CAST(MAX(t.time) AS INT),
			CAST(MAX(t.account_id) AS VARCHAR(64)),
			CAST(MAX(t.account_name) AS VARCHAR(64)),
			t.unique_chr_id,
			CAST(MAX(t.chr_id) AS VARCHAR(64)),
			CAST(MAX(t.chr_name) AS VARCHAR(64)),
			CAST(MAX(t.chr_lvl) AS SMALLINT),
			CAST(MAX(t.chr_lvl_vip) AS TINYINT),
			CAST(MAX(t.career) AS VARCHAR(16)),
			CAST(MAX(t.gender) AS VARCHAR(16)),
			CAST(MAX(t.device_id) AS VARCHAR(64)),
			CAST(MAX(t.model) AS VARCHAR(64)),
			CAST(MAX(t.os) AS VARCHAR(32)),
			CAST(MAX(t.carrier) AS VARCHAR(64)),
			CAST(MAX(t.network_type) AS VARCHAR(16)),
			CAST(MAX(t.resolution) AS VARCHAR(16)),
			CAST(t.product_id AS TINYINT)
		FROM tmp_raw_logins AS t
		LEFT JOIN raw_chrs AS c ON (
			t.product_id = c.product_id
			AND t.unique_chr_id = c.unique_chr_id
		)
		WHERE c.unique_chr_id IS NULL
		GROUP BY t.product_id, t.unique_chr_id
		`

	err = impala.Query(chrQuery)
	if err != nil {
		return err
	}

	err = impala.Compute("raw_chrs")
	if err != nil {
		return err
	}

	err = impala.Truncate(m.Table())
	if err != nil {
		return err
	}

	return nil
}

func init() {
	register("login", newLogin())
}
