package source

import (
	"fmt"

	"glowworm/module/helper"

	"gopkg.in/asaskevich/govalidator.v4"
)

// DwAccount 封装角色信息
type DwAccount struct {
	AccountID   string `json:"account_id" valid:"required,length(0|64)"`
	AccountName string `json:"account_name" valid:"-,length(0|64)"`
	UniqueChrID string
	ChrID       string `json:"chr_id" valid:"required,length(0|64)"`
	ChrName     string `json:"chr_name" valid:"required,length(0|64)"`
	ChrLevel    int32  `json:"chr_level" valid:"required,int32Validator"`
	ChrLevelVip int32  `json:"chr_level_vip" valid:"-,int32Validator"`
	Career      string `json:"career" valid:"-,length(0|16)"`
	Gender      string `json:"gender" valid:"-,length(0|16)"`
}

func (m *DwAccount) processAccount() error {
	_, err := govalidator.ValidateStruct(m)
	if err != nil {
		return err
	}

	return nil
}

func SetUniqueChrID(pid, plid, gid int32, cid string) string {
	md5str := helper.MD5(fmt.Sprintf("%d_%d_%d_%s", pid, plid, gid, cid))
	return md5str[8:24]
}
