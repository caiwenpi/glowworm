package sink

import (
	"glowworm/module/impala"
)

type Payment struct {
	Modeler
}

func newPayment() *Payment {
	m := new(Payment)
	m.dir = "payment"
	m.table = "tmp_raw_payments"
	return m
}

func (m *Payment) Load() error {
	var err error
	query1 := `
		INSERT INTO raw_payments PARTITION (pid, year)
		SELECT
			t.product_id,
			t.product_name,
			t.platform_id,
			t.platform_name,
			t.channel_id,
			t.channel_name,
			t.gameserver_id,
			t.gameserver_no,
			t.ip,
			t.region,
			t.the_date,
			t.the_date_time,
			t.hour,
			t.time,
			t.account_id,
			t.account_name,
			t.unique_chr_id,
			t.chr_id,
			t.chr_name,
			t.chr_lvl,
			t.chr_lvl_vip,
			t.career,
			t.gender,
			t.device_id,
			t.model,
			t.os,
			t.carrier,
			t.network_type,
			t.resolution,

			t.order_id,
			t.currency_type,
			t.currency_amount,
			t.pay_category,
			t.package_name,
			t.pay_channel,
			t.virtual_amount,

			CAST(t.product_id AS TINYINT),
			CAST(FROM_UNIXTIME(t.time, 'yy') AS TINYINT)
		FROM tmp_raw_payments AS t
		LEFT JOIN raw_payments AS p ON (
			t.product_id = p.product_id
			AND t.unique_chr_id = p.unique_chr_id
			AND t.order_id = p.order_id
		)
		WHERE p.order_id IS NULL
	`
	err = impala.Query(query1)
	if err != nil {
		return err
	}

	err = impala.Compute("raw_payments")
	if err != nil {
		return err
	}

	query2 := `
		INSERT INTO raw_payments_first PARTITION (pid, year)
		SELECT
			t.product_id,
			CAST(MAX(t.product_name) AS VARCHAR(32)),
			CAST(MAX(t.platform_id) AS SMALLINT),
			CAST(MAX(t.platform_name) AS VARCHAR(32)),
			CAST(MAX(t.channel_id) AS SMALLINT),
			CAST(MAX(t.channel_name) AS VARCHAR(32)),
			CAST(MAX(t.gameserver_id) AS INT),
			CAST(MAX(t.gameserver_no) AS INT),
			CAST(MAX(t.ip) AS VARCHAR(15)),
			CAST(MAX(t.region) AS VARCHAR(128)),
			CAST(MAX(t.the_date) AS TIMESTAMP),
			CAST(MAX(t.the_date_time) AS TIMESTAMP),
			CAST(MAX(t.hour) AS TINYINT),
			CAST(MAX(t.time) AS INT),
			CAST(MAX(t.account_id) AS VARCHAR(64)),
			CAST(MAX(t.account_name) AS VARCHAR(64)),
			t.unique_chr_id,
			CAST(MAX(t.chr_id) AS VARCHAR(64)),
			CAST(MAX(t.chr_name) AS VARCHAR(64)),
			CAST(MAX(t.chr_lvl) AS SMALLINT),
			CAST(MAX(t.chr_lvl_vip) AS TINYINT),
			CAST(MAX(t.career) AS VARCHAR(16)),
			CAST(MAX(t.gender) AS VARCHAR(16)),
			CAST(MAX(t.device_id) AS VARCHAR(64)),
			CAST(MAX(t.model) AS VARCHAR(64)),
			CAST(MAX(t.os) AS VARCHAR(32)),
			CAST(MAX(t.carrier) AS VARCHAR(64)),
			CAST(MAX(t.network_type) AS VARCHAR(16)),
			CAST(MAX(t.resolution) AS VARCHAR(16)),

			CAST(MAX(t.order_id) AS VARCHAR(120)),
			CAST(MAX(t.currency_type) AS VARCHAR(3)),
			CAST(MAX(t.currency_amount) AS DECIMAL(10,2)),
			CAST(MAX(t.pay_category) AS TINYINT),
			CAST(MAX(t.package_name) AS VARCHAR(64)),
			CAST(MAX(t.pay_channel) AS VARCHAR(64)),
			CAST(MAX(t.virtual_amount) AS INT),

			CAST(t.product_id AS TINYINT),
			CAST(MAX(FROM_UNIXTIME(t.time, 'yy')) AS TINYINT)
		FROM tmp_raw_payments AS t
		LEFT JOIN raw_payments_first AS f ON (
			t.product_id = f.product_id
			AND t.unique_chr_id = f.unique_chr_id
		)
		WHERE f.unique_chr_id IS NULL
		GROUP BY t.product_id, t.unique_chr_id
	`
	err = impala.Query(query2)
	if err != nil {
		return err
	}

	err = impala.Compute("raw_payments_first")
	if err != nil {
		return err
	}

	err = impala.Truncate(m.Table())
	if err != nil {
		return err
	}

	return nil
}

func init() {
	register("payment", newPayment())
}
