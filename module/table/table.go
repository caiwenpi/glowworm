package table

import (
	"glowworm/module/impala"

	"github.com/Sirupsen/logrus"
)

const (
	clmProduct string = "product"
	clmIPTime  string = "ip_time"
	clmAccount string = "account"
	clmDevice  string = "device"

	pttPYM string = "pym"
	pttPY  string = "py"
	pttP   string = "p"

	tmplRaw string = "raw"
	tmplTmp string = "tmp"
)

var columns map[string]string = map[string]string{
	clmProduct: `
		product_id TINYINT,
		product_name VARCHAR(32),
		platform_id SMALLINT,
		platform_name VARCHAR(32),
		channel_id SMALLINT,
		channel_name VARCHAR(32),
		gameserver_id INT,
		gameserver_no INT
	`,
	clmIPTime: `
		ip VARCHAR(15),
		region VARCHAR(128),
		the_date TIMESTAMP,
		the_date_time TIMESTAMP,
		hour TINYINT,
		time INT
	`,
	clmAccount: `
		account_id VARCHAR(64),
		account_name VARCHAR(64),
		unique_chr_id VARCHAR(16),
		chr_id VARCHAR(64),
		chr_name VARCHAR(64),
		chr_lvl SMALLINT,
		chr_lvl_vip TINYINT,
		career VARCHAR(16),
		gender VARCHAR(16)
	`,
	clmDevice: `
		device_id VARCHAR(64),
		model VARCHAR(64),
		os VARCHAR(32),
		carrier VARCHAR(64),
		network_type VARCHAR(16),
		resolution VARCHAR(16)
	`,
}

var partitions map[string]string = map[string]string{
	pttPYM: `
	  	pid TINYINT,
		year TINYINT,
		month TINYINT
	`,
	pttPY: `
	    pid TINYINT,
		year TINYINT
	`,
	pttP: `
		pid TINYINT
	`,
}

var templates map[string]string = map[string]string{
	tmplRaw: `
		CREATE TABLE IF NOT EXISTS gameadmindw.raw_%s (
			%s
		) PARTITIONED BY (
			%s
		) STORED AS PARQUET
	`,
	tmplTmp: `
		CREATE TABLE IF NOT EXISTS gameadmindw.tmp_raw_%s (
			%s
		)
		ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t' ESCAPED BY '"'
		STORED AS TEXTFILE
		LOCATION '/user/impala/warehouse/gameadmindw.db/tmp_raw_%s'
	`,
}

func Run(action string) error {
	switch action {
	case "create":
		create()
	case "truncate":
		truncate()
	case "drop":
		drop()
	default:
		logrus.Warn("What do you want to do?")
	}

	return nil
}

func create() {
	login()
	chr()
	session()
	payment()
	paymentfirst()
	virtualcurrency()
	virtualitem()
	levelup()
	tasklog()
	online()
	device()
	entry()
	customevent()
}

func truncate() {
	query := "SHOW TABLES"
	rows, err := impala.QueryAll(query)
	if err != nil {
		logrus.Error(err)
		return
	}
	for _, v := range rows {
		t, ok := v["name"].(string)
		if !ok {
			logrus.Warnf("Row name is not string: %v", v)
			continue
		}
		err = impala.Query("TRUNCATE TABLE " + t)
		if err != nil {
			logrus.Error(err)
		}
	}
}

func drop() {
	query := "SHOW TABLES"
	rows, err := impala.QueryAll(query)
	if err != nil {
		logrus.Error(err)
		return
	}
	for _, v := range rows {
		t, ok := v["name"].(string)
		if !ok {
			logrus.Warnf("Row name is not string: %v", v)
			continue
		}
		err = impala.Query("DROP TABLE " + t)
		if err != nil {
			logrus.Error(err)
		}
	}
}

func column(typ string) string {
	s, ok := columns[typ]
	if !ok {
		logrus.Errorf("unknown columns type: %s", typ)
		return ""
	}
	return s
}

func partition(typ string) string {
	s, ok := partitions[typ]
	if !ok {
		logrus.Errorf("unknown partition type: %s", typ)
		return ""
	}
	return s
}

func template(typ string) string {
	s, ok := templates[typ]
	if !ok {
		logrus.Errorf("unknow template type: %s", typ)
	}
	return s
}
