package model

import (
	"encoding/json"
	"fmt"
	"glowworm/module/httpapi"
	"net/url"
	"sync"

	"github.com/Sirupsen/logrus"
)

var VirtualCurrencyMu = new(sync.Mutex)

// VirtualCurrency 虚拟货币
type VirtualCurrency struct {
	VCID      uint   `json:"vc_id" gorm:"column:vc_id;primary_key"`
	ProductID uint   `json:"product_id" gorm:"column:product_id"`
	Name      string `json:"name" gorm:"column:name"`
}

// TableName 表名
func (VirtualCurrency) TableName() string {
	return "dw_virtual_currencies"
}

// GetVirtualCurrency 获取
func GetVirtualCurrency(pid uint, name string) (*VirtualCurrency, error) {
	m := newVirtualCurrency(pid, name)
	return m, GetModel(m)
}

func newVirtualCurrency(pid uint, name string) *VirtualCurrency {
	return &VirtualCurrency{
		ProductID: pid,
		Name:      name,
	}
}

func (m *VirtualCurrency) key() string {
	return fmt.Sprintf("virtual-currency-%d-%s", m.ProductID, m.Name)
}

func (m *VirtualCurrency) Isset() bool {
	return m.VCID != 0
}

func (m *VirtualCurrency) dbGet() {
	DB.Where(&VirtualCurrency{
		ProductID: m.ProductID,
		Name:      m.Name,
	}).First(&m)
}

func (m *VirtualCurrency) remoteCreate() error {
	VirtualCurrencyMu.Lock()
	defer VirtualCurrencyMu.Unlock()

	body, err := httpapi.PostForm("virtual-currency", url.Values{
		"product_id": {fmt.Sprint(m.ProductID)},
		"name":       {m.Name},
	})
	if err != nil {
		return err
	}

	tmp := new(VirtualCurrency)
	if err = json.Unmarshal(body, &tmp); err != nil {
		logrus.Warn(err)
	}
	m.VCID = tmp.VCID
	return nil
}

func (m *VirtualCurrency) errors() error {
	return fmt.Errorf("virtual currency not found product_id: %d, name: %s", m.ProductID, m.Name)
}
