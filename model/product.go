package model

import "fmt"

// Product 游戏表
type Product struct {
	ProductID uint   `json:"product_id" gorm:"column:product_id;primary_key"`
	Name      string `json:"name" gorm:"type:varchar(16);not null;unique"`
}

// TableName 指定表名
func (Product) TableName() string {
	return "products"
}

// GetProduct 根据名称获取游戏
func GetProduct(name string) (*Product, error) {
	m := newProduct(name)
	return m, GetModel(m)
}

func newProduct(name string) *Product {
	return &Product{
		Name: name,
	}
}

func (m *Product) key() string {
	return fmt.Sprintf("product-%s", m.Name)
}

func (m *Product) Isset() bool {
	return m.ProductID != 0
}

func (m *Product) dbGet() {
	DB.Where(&Product{
		Name: m.Name,
	}).First(&m)
}

func (m *Product) remoteCreate() error {
	return nil
}

func (m *Product) errors() error {
	return fmt.Errorf("product not found name: %s", m.Name)
}
