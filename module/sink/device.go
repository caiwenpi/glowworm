package sink

import (
	"glowworm/module/impala"
)

type Device struct {
	Modeler
}

func newDevice() *Device {
	m := new(Device)
	m.dir = "device"
	m.table = "tmp_raw_device"
	return m
}

func (m *Device) Load() error {
	var err error
	query := `
		INSERT INTO raw_device PARTITION (pid)
		SELECT
			t.*,
			CAST(t.product_id AS TINYINT)
		FROM tmp_raw_device AS t
		LEFT JOIN raw_device AS l ON (
			t.product_id = l.product_id
			AND t.platform_id = l.platform_id
			AND t.channel_id = l.channel_id
			AND t.device_id = l.device_id
		)
		WHERE l.device_id IS NULL
	`

	err = impala.Query(query)
	if err != nil {
		return err
	}

	err = impala.Compute("raw_device")
	if err != nil {
		return err
	}

	err = impala.Truncate(m.Table())
	if err != nil {
		return err
	}

	return nil
}

func init() {
	register("device", newDevice())
}
