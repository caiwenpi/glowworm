package model

import (
	"encoding/json"
	"fmt"
	"glowworm/module/httpapi"
	"net/url"
	"sync"

	"github.com/Sirupsen/logrus"
)

var VirtualCurrencyProjectMu = new(sync.Mutex)

// VirtualCurrencyProject 虚拟货币项目
type VirtualCurrencyProject struct {
	VPID      uint   `json:"vp_id" gorm:"column:vp_id;primary_key"`
	ProductID uint   `json:"product_id" gorm:"column:product_id"`
	Name      string `json:"name" gorm:"column:name"`
}

// TableName 表名
func (VirtualCurrencyProject) TableName() string {
	return "dw_virtual_currency_project"
}

// GetVirtualCurrencyProject 获取
func GetVirtualCurrencyProject(pid uint, name string) (*VirtualCurrencyProject, error) {
	m := newVirtualCurrencyProject(pid, name)
	return m, GetModel(m)
}

func newVirtualCurrencyProject(pid uint, name string) *VirtualCurrencyProject {
	return &VirtualCurrencyProject{
		ProductID: pid,
		Name:      name,
	}
}

func (m *VirtualCurrencyProject) key() string {
	return fmt.Sprintf("virtual-currency-project-%d-%s", m.ProductID, m.Name)
}

func (m *VirtualCurrencyProject) Isset() bool {
	return m.VPID != 0
}

func (m *VirtualCurrencyProject) dbGet() {
	DB.Where(&VirtualCurrencyProject{
		ProductID: m.ProductID,
		Name:      m.Name,
	}).First(&m)
}

func (m *VirtualCurrencyProject) remoteCreate() error {
	VirtualCurrencyProjectMu.Lock()
	defer VirtualCurrencyProjectMu.Unlock()

	body, err := httpapi.PostForm("virtual-project", url.Values{
		"product_id": {fmt.Sprint(m.ProductID)},
		"name":       {m.Name},
	})
	if err != nil {
		return err
	}

	tmp := new(VirtualCurrencyProject)
	if err = json.Unmarshal(body, &tmp); err != nil {
		logrus.Warn(err)
	}
	m.VPID = tmp.VPID
	return nil
}

func (m *VirtualCurrencyProject) errors() error {
	return fmt.Errorf("virtual currency project not found product_id: %d, name: %s", m.ProductID, m.Name)
}
