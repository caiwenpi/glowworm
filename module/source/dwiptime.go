package source

import (
	"glowworm/module/setting"
	"net"
	"path"
	"time"

	"github.com/asaskevich/govalidator"
	geoip2 "github.com/oschwald/geoip2-golang"
)

// DwIPTime 封装IP时间信息
type DwIPTime struct {
	IP       string `json:"ip" valid:"ip"`
	Region   string
	TheDate  string
	DateTime string `json:"date_time" valid:"required"`
	Hour     int
	Time     int64
}

func (m *DwIPTime) processIPTime() error {
	_, err := govalidator.ValidateStruct(m)
	if err != nil {
		return err
	}
	if m.IP != "" {
		db, err := geoip2.Open(path.Join(setting.RootPath, "conf/GeoLite2-City.mmdb"))
		if err != nil {
			return err
		}
		defer db.Close()

		ip := net.ParseIP(m.IP)
		rc, err := db.City(ip)
		if err != nil {
			return err
		}

		if len(rc.Subdivisions) > 0 {
			m.Region = rc.Subdivisions[0].Names["zh-CN"]
		}
	}

	loc, err := time.LoadLocation("PRC")
	if err != nil {
		return err
	}

	t, err := time.ParseInLocation(setting.TimestampFormat, m.DateTime, loc)
	if err != nil {
		return err
	}
	m.TheDate = t.Format(setting.DateFormat)
	m.Hour = t.Hour()
	m.Time = t.Unix()

	return nil
}
