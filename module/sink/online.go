package sink

import (
	"glowworm/module/impala"
)

type Online struct {
	Modeler
}

func newOnline() *Online {
	m := new(Online)
	m.dir = "online"
	m.table = "tmp_raw_online"
	return m
}

func (m *Online) Load() error {
	var err error
	query := `
		INSERT INTO raw_online PARTITION (pid, year, month)
		SELECT
			t.*,
			CAST(t.product_id AS TINYINT),
			CAST(FROM_UNIXTIME(t.time, 'yy') AS TINYINT),
			CAST(FROM_UNIXTIME(t.time, 'MM') AS TINYINT)
		FROM tmp_raw_online AS t
		LEFT JOIN raw_online AS l ON (
			t.product_id = l.product_id
			AND t.platform_id = l.platform_id
			AND t.channel_id = l.channel_id
			AND t.gameserver_id = l.gameserver_id
			AND t.time = l.time
		)
		WHERE l.num IS NULL
	`

	err = impala.Query(query)
	if err != nil {
		return err
	}

	err = impala.Compute("raw_online")
	if err != nil {
		return err
	}

	err = impala.Truncate(m.Table())
	if err != nil {
		return err
	}

	return nil
}

func init() {
	register("online", newOnline())
}
