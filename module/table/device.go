package table

import (
	"fmt"
	"glowworm/module/impala"
	"strings"

	"github.com/Sirupsen/logrus"
)

func device() {
	var err error
	table := "device"
	sl := []string{
		column(clmDevice),
		`
		product_id TINYINT,
		product_name VARCHAR(32),
		platform_id SMALLINT,
		platform_name VARCHAR(32),
		channel_id SMALLINT,
		channel_name VARCHAR(32)
		`,
		column(clmIPTime),
	}

	clms := strings.Join(sl, ",")

	raw := fmt.Sprintf(template(tmplRaw), table, clms, partition(pttP))

	err = impala.Query(raw)
	if err != nil {
		logrus.Error(err)
	}

	tmp := fmt.Sprintf(template(tmplTmp), table, clms, table)
	err = impala.Query(tmp)
	if err != nil {
		logrus.Error(err)
	}
}
