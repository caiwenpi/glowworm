package main

import (
	"os"
	"time"

	"glowworm/cmd"

	"github.com/Sirupsen/logrus"
	"github.com/urfave/cli"
)

const version = "0.1.1+dev"

func main() {
	app := cli.NewApp()
	app.Name = "glowworm"
	app.Usage = "Parse flume log and load into impala"
	app.Version = version
	app.Compiled = time.Now()
	app.Commands = []cli.Command{
		cmd.Parse,
		cmd.Load,
		cmd.Table,
	}
	err := app.Run(os.Args)
	if err != nil {
		logrus.Errorf("Run app failed with %s: %v", os.Args, err)
	}
}
