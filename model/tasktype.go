package model

import (
	"encoding/json"
	"fmt"
	"glowworm/module/httpapi"
	"net/url"
	"sync"

	"github.com/Sirupsen/logrus"
)

var TaskTypeMu = new(sync.Mutex)

// TaskType 任务类型
type TaskType struct {
	UniqueTypeID uint   `json:"unique_type_id" gorm:"column:unique_type_id;primary_key"`
	ProductID    uint   `json:"product_id" gorm:"column:product_id"`
	TypeID       int32  `json:"type_id" gorm:"column:type_id"`
	Name         string `json:"name" gorm:"column:name"`
}

// TableName 表名
func (TaskType) TableName() string {
	return "dw_task_type"
}

// GetTaskType 获取
func GetTaskType(pid uint, typeID int32, name string) (*TaskType, error) {
	m := newTaskType(pid, typeID, name)
	return m, GetModel(m)
}

func newTaskType(pid uint, typeID int32, name string) *TaskType {
	return &TaskType{
		ProductID: pid,
		TypeID:    typeID,
		Name:      name,
	}
}

func (m *TaskType) key() string {
	return fmt.Sprintf("task-type-%d-%d-%s", m.ProductID, m.TypeID, m.Name)
}

func (m *TaskType) Isset() bool {
	return m.UniqueTypeID != 0
}

func (m *TaskType) dbGet() {
	DB.Where(&TaskType{
		ProductID: m.ProductID,
		TypeID:    m.TypeID,
		Name:      m.Name,
	}).First(&m)
}

func (m *TaskType) remoteCreate() error {
	TaskTypeMu.Lock()
	defer TaskTypeMu.Unlock()

	body, err := httpapi.PostForm("task-type", url.Values{
		"product_id": {fmt.Sprint(m.ProductID)},
		"type_id":    {fmt.Sprint(m.TypeID)},
		"name":       {m.Name},
	})
	if err != nil {
		return err
	}

	tmp := new(TaskType)
	if err = json.Unmarshal(body, &tmp); err != nil {
		logrus.Warn(err)
	}
	m.UniqueTypeID = tmp.UniqueTypeID
	return nil
}

func (m *TaskType) errors() error {
	return fmt.Errorf("task type not found product_id: %d, type_id: %d, name: %s", m.ProductID, m.TypeID, m.Name)
}
