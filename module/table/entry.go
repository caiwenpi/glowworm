package table

import (
	"fmt"
	"glowworm/module/impala"
	"strings"

	"github.com/Sirupsen/logrus"
)

func entry() {
	var err error
	table := "entry"
	sl := []string{
		`
		product_id TINYINT,
		product_name VARCHAR(32),
		platform_id SMALLINT,
		platform_name VARCHAR(32),
		channel_id SMALLINT,
		channel_name VARCHAR(32)
		`,
		column(clmIPTime),
		`
		unique_account_id VARCHAR(16),
		account_id VARCHAR(64),
		account_name VARCHAR(64),
		step TINYINT
		`,
	}

	clms := strings.Join(sl, ",")

	raw := fmt.Sprintf(template(tmplRaw), table, clms, partition(pttPY))

	err = impala.Query(raw)
	if err != nil {
		logrus.Error(err)
	}

	tmp := fmt.Sprintf(template(tmplTmp), table, clms, table)
	err = impala.Query(tmp)
	if err != nil {
		logrus.Error(err)
	}
}
