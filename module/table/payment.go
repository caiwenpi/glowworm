package table

import (
	"fmt"
	"glowworm/module/impala"
	"strings"

	"github.com/Sirupsen/logrus"
)

func payment() {
	var err error
	table := "payments"
	sl := []string{
		column(clmProduct),
		column(clmIPTime),
		column(clmAccount),
		column(clmDevice),
		`
		order_id VARCHAR(120),
		currency_type VARCHAR(3),
		currency_amount DECIMAL(10,2),
		pay_category TINYINT,
		package_name VARCHAR(64),
		pay_channel VARCHAR(64),
		virtual_amount INT
		`,
	}

	clms := strings.Join(sl, ",")

	raw := fmt.Sprintf(template(tmplRaw), table, clms, partition(pttPY))

	err = impala.Query(raw)
	if err != nil {
		logrus.Error(err)
	}

	tmp := fmt.Sprintf(template(tmplTmp), table, clms, table)
	err = impala.Query(tmp)
	if err != nil {
		logrus.Error(err)
	}
}

func paymentfirst() {
	var err error
	table := "payments_first"
	sl := []string{
		column(clmProduct),
		column(clmIPTime),
		column(clmAccount),
		column(clmDevice),
		`
		order_id VARCHAR(120),
		currency_type VARCHAR(3),
		currency_amount DECIMAL(10,2),
		pay_category TINYINT,
		package_name VARCHAR(64),
		pay_channel VARCHAR(64),
		virtual_amount INT
		`,
	}

	clms := strings.Join(sl, ",")

	raw := fmt.Sprintf(template(tmplRaw), table, clms, partition(pttPY))

	err = impala.Query(raw)
	if err != nil {
		logrus.Error(err)
	}
}
