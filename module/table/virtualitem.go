package table

import (
	"fmt"
	"glowworm/module/impala"
	"strings"

	"github.com/Sirupsen/logrus"
)

func virtualitem() {
	var err error
	table := "virtual_items"
	sl := []string{
		column(clmProduct),
		column(clmIPTime),
		column(clmAccount),
		`
		item_unique_id INT,
		item_id VARCHAR(64),
		item_name VARCHAR(64),
		type_id INT,
		type_name VARCHAR(64),
		op_type TINYINT,
		op_count INT
		`,
	}

	clms := strings.Join(sl, ",")

	raw := fmt.Sprintf(template(tmplRaw), table, clms, partition(pttPYM))

	err = impala.Query(raw)
	if err != nil {
		logrus.Error(err)
	}

	tmp := fmt.Sprintf(template(tmplTmp), table, clms, table)
	err = impala.Query(tmp)
	if err != nil {
		logrus.Error(err)
	}
}
