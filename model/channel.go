package model

import "fmt"

// Channel 渠道信息
type Channel struct {
	ChannelID uint   `json:"channel_id" gorm:"column:channel_id;primary_key"`
	ProductID uint   `json:"product_id" gorm:"column:product_id"`
	Name      string `json:"name" gorm:"column:name;type:varchar(16);not null"`
}

// TableName 指定表名
func (Channel) TableName() string {
	return "channels"
}

// GetChannel 获取 pid name 的渠道
func GetChannel(pid uint, name string) (*Channel, error) {
	m := newChannel(pid, name)
	return m, GetModel(m)
}

func newChannel(pid uint, name string) *Channel {
	return &Channel{
		ProductID: pid,
		Name:      name,
	}
}

func (m *Channel) key() string {
	return fmt.Sprintf("channel-%d-%s", m.ProductID, m.Name)
}

func (m *Channel) Isset() bool {
	return m.ChannelID != 0
}

func (m *Channel) dbGet() {
	DB.Where(&Channel{
		ProductID: m.ProductID,
		Name:      m.Name,
	}).First(&m)
}

func (m *Channel) remoteCreate() error {
	return nil
}

func (m *Channel) errors() error {
	return fmt.Errorf("channel not found product_id: %d, name: %s", m.ProductID, m.Name)
}
