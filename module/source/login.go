package source

import (
	"bytes"
	"glowworm/module/csvs"

	"gopkg.in/asaskevich/govalidator.v4"
)

// Login 登录日志信息
type Login struct {
	DwProduct
	DwIPTime
	DwAccount
	DwDevice

	SessionID string `json:"session_id" valid:"required,length(0|32)"`
}

func NewLogin() ModelInterface {
	return new(Login)
}

// Validate 验证数据
func (m *Login) Validate() error {
	var err error
	_, err = govalidator.ValidateStruct(m)
	if err != nil {
		return err
	}

	err = m.processProduct()
	if err != nil {
		return err
	}
	err = m.processIPTime()
	if err != nil {
		return err
	}
	err = m.processAccount()
	if err != nil {
		return err
	}
	m.UniqueChrID = SetUniqueChrID(m.ProductID, m.PlatformID, m.GameserverID, m.ChrID)

	err = m.processDevice()
	if err != nil {
		return err
	}

	return nil
}

// Csv 准备上传数据
func (m *Login) Csv() (string, error) {
	var s string
	msg := bytes.NewBufferString(s)
	sl := []*Login{}
	sl = append(sl, m)
	if err := csvs.MarshalWithoutHeaders(&sl, msg); err != nil {
		return "", err
	}
	return msg.String(), nil
}

func init() {
	register("login", NewLogin)
}
