package sink

import (
	"fmt"
	"glowworm/module/hdfs"
	"glowworm/module/impala"
	"time"

	"os"

	"github.com/Sirupsen/logrus"
)

type Model interface {
	Dir() string
	Table() string
	Load() error
}

var adapters = make(map[string]Model)

func Run(adapter string) error {
	logrus.Infof("Begin load %s", adapter)
	defer func(t time.Time) {
		logrus.Infof("Finish load, duration %s", time.Since(t).String())
	}(time.Now())

	m, ok := adapters[adapter]
	if !ok {
		return fmt.Errorf("unknown adapter %s", adapter)
	}

	var err error
	var fs []os.FileInfo
	fs, err = hdfs.ReadDir(m.Dir())
	if err != nil {
		return err
	}

	if len(fs) < 1 {
		logrus.Info("Found no file in HDFS")
		return nil
	}

	err = impala.LoadToTemp(m.Dir(), m.Table())
	if err != nil {
		return err
	}
	return m.Load()
}

func register(name string, m Model) {
	adapters[name] = m
}

type Modeler struct {
	dir   string
	table string
}

func (m *Modeler) Dir() string {
	return m.dir
}

func (m *Modeler) Table() string {
	return m.table
}
