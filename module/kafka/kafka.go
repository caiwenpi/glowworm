package kafka

import (
	"github.com/Shopify/sarama"
	"github.com/Sirupsen/logrus"
)

var (
	Producer sarama.SyncProducer
)

func NewMassage(topic, value string) *sarama.ProducerMessage {
	return &sarama.ProducerMessage{
		Topic: topic,
		Key:   sarama.StringEncoder(topic),
		Value: sarama.ByteEncoder(value),
	}
}

func SendMsgs(msgs []*sarama.ProducerMessage) error {
	logrus.Debugf("Send msgs count: %d", len(msgs))
	if len(msgs) == 0 {
		return nil
	}
	return Producer.SendMessages(msgs)
}
