package table

import (
	"fmt"
	"glowworm/module/impala"
	"strings"

	"github.com/Sirupsen/logrus"
)

func session() {
	var err error
	table := "sessions"
	sl := []string{
		column(clmProduct),
		column(clmIPTime),
		column(clmAccount),
		`
		session_id VARCHAR(32),
		time_span INT
		`,
	}

	clms := strings.Join(sl, ",")

	raw := fmt.Sprintf(template(tmplRaw), table, clms, partition(pttPYM))

	err = impala.Query(raw)
	if err != nil {
		logrus.Error(err)
	}

	sl = []string{
		column(clmProduct),
		column(clmIPTime),
		column(clmAccount),
		`
		session_id VARCHAR(32)
		`,
	}

	clms = strings.Join(sl, ",")

	tmp := fmt.Sprintf(template(tmplTmp), table, clms, table)
	err = impala.Query(tmp)
	if err != nil {
		logrus.Error(err)
	}
}
