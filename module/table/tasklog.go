package table

import (
	"fmt"
	"glowworm/module/impala"
	"strings"

	"github.com/Sirupsen/logrus"
)

func tasklog() {
	var err error
	table := "task_log"
	sl := []string{
		column(clmProduct),
		column(clmIPTime),
		column(clmAccount),
		`
		operate TINYINT,
		unique_task_id INT,
		task_id INT,
		name VARCHAR(32),
		unique_type_id INT,
		type_id INT,
		type VARCHAR(32),
		result TINYINT,
		reason VARCHAR(255),
		session_id VARCHAR(32)
		`,
	}

	clms := strings.Join(sl, ",")

	raw := fmt.Sprintf(template(tmplRaw), table, clms, partition(pttPYM))

	err = impala.Query(raw)
	if err != nil {
		logrus.Error(err)
	}

	tmp := fmt.Sprintf(template(tmplTmp), table, clms, table)
	err = impala.Query(tmp)
	if err != nil {
		logrus.Error(err)
	}
}
