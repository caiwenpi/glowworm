package model

import (
	"encoding/json"
	"glowworm/module/redis"

	"github.com/Sirupsen/logrus"
	"github.com/jinzhu/gorm"
)

// DB 指定DB
var DB *gorm.DB

// Model interface
type Model interface {
	key() string
	Isset() bool
	dbGet()
	remoteCreate() error
	errors() error
}

// GetModel 获取 model 值
func GetModel(m Model) error {
	if err := redisGet(m); err != nil {
		return err
	}

	if !m.Isset() {
		m.dbGet()
		if !m.Isset() {
			if err := m.remoteCreate(); err != nil {
				return err
			}
			if !m.Isset() {
				return m.errors()
			}
		}
		go redisSet(m)
	}
	return nil
}

func redisGet(m Model) error {
	s, err := redis.Get(m.key())
	if err != nil {
		return err
	}
	//logrus.Debugf("Redis get key: %s value: %s", m.key(), s)

	if s == "" {
		return nil
	}
	return json.Unmarshal([]byte(s), &m)
}

func redisSet(m Model) {
	s, err := json.Marshal(m)
	if err != nil {
		logrus.Error(err)
		return
	}
	if err = redis.Set(m.key(), string(s)); err != nil {
		logrus.Error(err)
		return
	}
	//logrus.Debugf("Redis set key: %s, value: %s", m.key(), string(s))
	return
}
