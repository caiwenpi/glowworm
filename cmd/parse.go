package cmd

import (
	"fmt"

	"glowworm/module/helper"
	"glowworm/module/setting"
	"glowworm/module/source"

	"time"

	"github.com/urfave/cli"
)

// Parse the source data and send to hdfs
var Parse = cli.Command{
	Name:   "parse",
	Action: runParse,
	Before: func(c *cli.Context) error {
		setting.NewContext(c)
		setting.NewDBService()
		setting.NewRedisService()
		setting.NewKafkaService()
		setting.NewHTTPApi()
		return nil
	},
	After: func(c *cli.Context) error {
		setting.CloseDBService()
		setting.CloseKafkaService()
		return nil
	},
	Flags: []cli.Flag{
		cli.StringFlag{
			Name: "category, c",
		},
	},
}

func runParse(c *cli.Context) error {
	category := c.String("category")

	if !helper.ContainsString(setting.DataCategories, category) {
		return fmt.Errorf("unknown category %s", category)
	}

	for {
		if fileCount, err := source.Run(category); err != nil {
			return err
		} else if fileCount > 2 {
			time.Sleep(1 * time.Second)
		} else {
			time.Sleep(setting.RunInterval * time.Second)
		}
	}
	return nil
}
