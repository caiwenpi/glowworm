package httpapi

import (
	"fmt"
	"glowworm/module/helper"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"
)

var (
	Url string
	Key string
)

func PostForm(subUrl string, data url.Values) ([]byte, error) {
	t := fmt.Sprintf("%d", time.Now().Unix())
	md5str := helper.MD5(fmt.Sprintf("%s-%s", Key, t))
	values := url.Values{
		"time": {t},
		"sign": {md5str[8:24]},
	}

	for k, v := range data {
		values[k] = v
	}
	resp, err := http.PostForm(Url+subUrl, values)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("Respone error: %v", resp)
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	return body, nil
}
