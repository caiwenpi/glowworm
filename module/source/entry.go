package source

import (
	"fmt"
	"glowworm/model"
	"glowworm/module/csvs"
	"glowworm/module/helper"

	"bytes"

	"gopkg.in/asaskevich/govalidator.v4"
)

// Entry 加载日志信息
type Entry struct {
	ProductID    int32
	ProductName  string `json:"product_name" valid:"required,length(0|32)"`
	PlatformID   int32
	PlatformName string `json:"platform_name" valid:"required,length(0|32)"`
	ChannelID    int32
	ChannelName  string `json:"channel_name" valid:"required,length(0|32)"`

	DwIPTime

	UniqueAccountID string
	AccountID       string `json:"account_id" valid:"required,length(0|64)"`
	AccountName     string `json:"account_name" valid:"-,length(0|64)"`
	Step            int32  `json:"step" valid:"required"`
}

func NewEntry() ModelInterface {
	return new(Entry)
}

// Validate 验证数据
func (m *Entry) Validate() error {
	var err error
	var product *model.Product
	var platform *model.Platform
	var channel *model.Channel

	_, err = govalidator.ValidateStruct(m)
	if err != nil {
		return err
	}
	product, err = model.GetProduct(m.ProductName)
	if err != nil {
		return err
	}
	m.ProductID = int32(product.ProductID)

	platform, err = model.GetPlatform(product.ProductID, m.PlatformName)
	if err != nil {
		return err
	}
	m.PlatformID = int32(platform.PlatformID)

	channel, err = model.GetChannel(product.ProductID, m.ChannelName)
	if err != nil {
		return err
	}
	m.ChannelID = int32(channel.ChannelID)

	err = m.processIPTime()
	if err != nil {
		return err
	}

	md5str := helper.MD5(fmt.Sprintf("%d_%d_%s", m.ProductID, m.PlatformID, m.AccountID))
	m.UniqueAccountID = md5str[8:24]

	return nil
}

// Csv 准备上传数据
func (m *Entry) Csv() (string, error) {
	var s string
	msg := bytes.NewBufferString(s)
	sl := []*Entry{}
	sl = append(sl, m)
	if err := csvs.MarshalWithoutHeaders(&sl, msg); err != nil {
		return "", err
	}
	return msg.String(), nil
}

func init() {
	register("entry", NewEntry)
}
