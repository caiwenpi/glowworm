package source

import "github.com/asaskevich/govalidator"

// DwDevice 封装设备信息
type DwDevice struct {
	DeviceID    string `json:"device_id" valid:"required,length(0|64)"`
	DeviceType  string `json:"device_type" valid:"_,length(0|64)"`
	Os          string `json:"os" valid:"_,length(0|32)"`
	Carrier     string `json:"carrier" valid:"_,length(0|64)"`
	NetworkType string `json:"network_type" valid:"_,length(0|16)"`
	Resolution  string `json:"resolution" valid:"-,length(0|16)"`
}

func (m *DwDevice) processDevice() error {
	_, err := govalidator.ValidateStruct(m)
	if err != nil {
		return err
	}

	return nil
}
