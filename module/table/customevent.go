package table

import (
	"fmt"
	"glowworm/module/impala"
	"strings"

	"github.com/Sirupsen/logrus"
)

func customevent() {
	var err error
	table := "custom_event"
	sl := []string{
		column(clmProduct),
		column(clmIPTime),
		column(clmAccount),
		`
		event_id INT,
		event_name VARCHAR(32),
		comments STRING,
 		session_id VARCHAR(32)
		`,
	}

	clms := strings.Join(sl, ",")
	ps := partition(pttPYM) + ",eid INT"
	raw := fmt.Sprintf(template(tmplRaw), table, clms, ps)

	err = impala.Query(raw)
	if err != nil {
		logrus.Error(err)
	}

	tmp := fmt.Sprintf(template(tmplTmp), table, clms, table)
	err = impala.Query(tmp)
	if err != nil {
		logrus.Error(err)
	}
}
