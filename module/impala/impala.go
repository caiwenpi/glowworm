package impala

import (
	"fmt"
	"glowworm/module/hdfs"
	"path"
	"time"

	"github.com/Sirupsen/logrus"
	"github.com/koblas/impalathing"
)

var (
	// Conn is impalathing connection, set in setting module
	Conn *impalathing.Connection
)

func query(q string) (rs impalathing.RowSet, err error) {
	for i := 0; i < 3; i++ {
		logrus.Debug(q)
		rs, err = Conn.Query(q)
		if err == nil {
			_, err = rs.Wait()
			if err == nil {
				return
			}
			return
		}
		time.Sleep(3 * time.Second)
	}
	return
}

// QueryAll is query and return raws and error
func QueryAll(q string) ([]map[string]interface{}, error) {
	res, err := query(q)
	if err != nil {
		return nil, err
	}
	return res.FetchAll(), nil
}

// Query is only query
func Query(q string) error {
	_, err := query(q)
	return err
}

// LoadToTemp is load the HDFS path data into temp table
func LoadToTemp(dir, table string) error {
	q := fmt.Sprintf("LOAD DATA INPATH '%s' INTO TABLE %s", path.Join(hdfs.Path, dir), table)
	_, err := query(q)
	return err
}

// Compute is compute table
func Compute(table string) error {
	q := fmt.Sprintf("COMPUTE INCREMENTAL STATS %s", table)
	_, err := query(q)
	return err
}

// Truncate is use insert overwrite to truncate table
func Truncate(table string) error {
	q := fmt.Sprintf("INSERT OVERWRITE %s SELECT * FROM %s LIMIT 0", table, table)
	_, err := query(q)
	return err
}
