package source

import (
	"bytes"
	"glowworm/model"
	"glowworm/module/csvs"

	"gopkg.in/asaskevich/govalidator.v4"
)

// CustomEvent 自定义日志信息
type CustomEvent struct {
	DwProduct
	DwIPTime
	DwAccount

	EventID   int32
	EventName string `json:"event_name" valid:"required,length(0|32)"`
	Comments  string `json:"comments"`
	SessionID string `json:"session_id" valid:"required,length(0|32)"`
}

func NewCustomEvent() ModelInterface {
	return new(CustomEvent)
}

// Validate 验证数据
func (m *CustomEvent) Validate() error {
	var err error
	_, err = govalidator.ValidateStruct(m)
	if err != nil {
		return err
	}

	err = m.processProduct()
	if err != nil {
		return err
	}
	err = m.processIPTime()
	if err != nil {
		return err
	}
	err = m.processAccount()
	if err != nil {
		return err
	}
	m.UniqueChrID = SetUniqueChrID(m.ProductID, m.PlatformID, m.GameserverID, m.ChrID)

	var evt *model.CustomEvent
	evt, err = model.GetCustomEvent(uint(m.ProductID), m.EventName)
	if err != nil {
		return err
	}
	m.EventID = int32(evt.EventID)

	return nil
}

// Csv 准备上传数据
func (m *CustomEvent) Csv() (string, error) {
	var s string
	msg := bytes.NewBufferString(s)
	sl := []*CustomEvent{}
	sl = append(sl, m)
	if err := csvs.MarshalWithoutHeaders(&sl, msg); err != nil {
		return "", err
	}
	return msg.String(), nil
}

func init() {
	register("custom-event", NewCustomEvent)
}
