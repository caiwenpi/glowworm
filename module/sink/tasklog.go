package sink

import (
	"glowworm/module/impala"
)

type TaskLog struct {
	Modeler
}

func newTaskLog() *TaskLog {
	m := new(TaskLog)
	m.dir = "task-log"
	m.table = "tmp_raw_task_log"
	return m
}

func (m *TaskLog) Load() error {
	var err error
	query := `
		INSERT INTO raw_task_log PARTITION (pid, year, month)
		SELECT
			t.product_id,
			t.product_name,
			t.platform_id,
			t.platform_name,
			t.channel_id,
			t.channel_name,
			t.gameserver_id,
			t.gameserver_no,
			t.ip,
			t.region,
			t.the_date,
			t.the_date_time,
			t.hour,
			t.time,
			t.account_id,
			t.account_name,
			t.unique_chr_id,
			t.chr_id,
			t.chr_name,
			t.chr_lvl,
			t.chr_lvl_vip,
			t.career,
			t.gender,

			t.operate,
			t.unique_task_id,
			t.task_id,
			t.name,
			t.unique_type_id,
			t.type_id,
			t.type,
			t.result,
			t.reason,
			t.session_id,

			CAST(t.product_id AS TINYINT),
			CAST(FROM_UNIXTIME(t.time, 'yy') AS TINYINT),
			CAST(FROM_UNIXTIME(t.time, 'MM') AS TINYINT)
		FROM tmp_raw_task_log AS t
	`

	err = impala.Query(query)
	if err != nil {
		return err
	}

	err = impala.Compute("raw_task_log")
	if err != nil {
		return err
	}

	err = impala.Truncate(m.Table())
	if err != nil {
		return err
	}

	return nil
}

func init() {
	register("task-log", newTaskLog())
}
