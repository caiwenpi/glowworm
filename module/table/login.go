package table

import (
	"fmt"
	"glowworm/module/impala"
	"strings"

	"github.com/Sirupsen/logrus"
)

func login() {
	var err error
	table := "logins"
	sl := []string{
		column(clmProduct),
		column(clmIPTime),
		column(clmAccount),
		column(clmDevice),
		`
		session_id VARCHAR(32)
		`,
	}

	clms := strings.Join(sl, ",")

	raw := fmt.Sprintf(template(tmplRaw), table, clms, partition(pttPYM))

	err = impala.Query(raw)
	if err != nil {
		logrus.Error(err)
	}

	tmp := fmt.Sprintf(template(tmplTmp), table, clms, table)
	err = impala.Query(tmp)
	if err != nil {
		logrus.Error(err)
	}
}

func chr() {
	var err error
	table := "chrs"
	sl := []string{
		column(clmProduct),
		column(clmIPTime),
		column(clmAccount),
		column(clmDevice),
	}
	clms := strings.Join(sl, ",")
	raw := fmt.Sprintf(template(tmplRaw), table, clms, partition(pttP))

	err = impala.Query(raw)
	if err != nil {
		logrus.Error(err)
	}
}
