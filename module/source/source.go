package source

import (
	"bufio"
	"encoding/json"
	"fmt"
	"os"
	"path"
	"strings"
	"sync"
	"time"

	"glowworm/module/helper"
	"glowworm/module/kafka"
	"glowworm/module/setting"

	"github.com/Shopify/sarama"
	"github.com/Sirupsen/logrus"
)

var (
	delMu  = new(sync.Mutex)
	fileMu = new(sync.Mutex)
)

const (
	ResultSuccess int = iota
	ResultFailedParse
	ResultFailedValidate
	ResultFailedCsv
)

type Job struct {
	content string
}

type Result struct {
	source string
	csv    string
	topic  string
	end    int
}

type Massage struct {
	topic string
	msg   string
}

type Modeler func() ModelInterface

// ModelInterface 接口不同日志类型不同实现
type ModelInterface interface {
	Validate() error
	Csv() (string, error)
}

var (
	adapters = make(map[string]Modeler)

	// log dir
	dirs = map[string]string{
		"login":            "login",
		"device":           "device",
		"entry":            "entry",
		"level-up":         "level-up",
		"online":           "online",
		"payment":          "payment",
		"session":          "session",
		"task-log":         "task-log",
		"virtual-currency": "virtual-currency",
		"virtual-item":     "virtual-item",
		"custom-event":     "custom-event",
	}

	// flume topic
	topics = map[string]string{
		"login":            "login",
		"device":           "device",
		"entry":            "entry",
		"level-up":         "level-up",
		"online":           "online",
		"payment":          "payment",
		"session":          "session",
		"task-log":         "task-log",
		"virtual-currency": "virtual-currencies",
		"virtual-item":     "virtual-items",
		"custom-event":     "custom-event",
	}

	allRowCount         int32
	successCount        int32
	failedParseCount    int32
	failedValidateCount int32
	failedCsvCount      int32
)

// Run 请求的开始
func Run(adapter string) (int, error) {
	var fileCount int
	allRowCount, successCount, failedParseCount, failedValidateCount, failedCsvCount = 0, 0, 0, 0, 0
	logrus.Infof("Run parse: %s", adapter)
	defer func(t time.Time) {
		if allRowCount != 0 {
			logrus.Infof("Finish parse %d rows, duration %s, success %d, parse %d, validate %d, csv %d",
				allRowCount, time.Since(t).String(), successCount, failedParseCount, failedValidateCount, failedCsvCount)
		}
	}(time.Now())

	_, ok := adapters[adapter]
	if !ok {
		return fileCount, fmt.Errorf("unknown adapter %s", adapter)
	}

	if _, b := topics[adapter]; !b {
		return fileCount, fmt.Errorf("unknown adapter topic: %s", adapter)
	}

	dir, ok := dirs[adapter]
	if !ok {
		return fileCount, fmt.Errorf("adapter dir no set: %s", adapter)
	}
	sourcePath := path.Join(setting.SourcePath, dir)
	if !helper.IsDir(sourcePath) {
		return fileCount, fmt.Errorf("path is not a dir: %s", sourcePath)
	}

	backupPath := path.Join(setting.BackupPath, dir)
	if !helper.IsDir(backupPath) {
		if err := os.Mkdir(backupPath, 0777); err != nil {
			return fileCount, err
		}
	}

	fls, err := helper.GetFilesPathSortByMTime(sourcePath)
	if err != nil {
		return fileCount, err
	}
	fileCount = len(fls)
	if fileCount < 2 {
		logrus.Warnf("Less 2 files found: %s", sourcePath)
		return fileCount, nil
	}
	logrus.Infof("Found %d files", fileCount)

	// gogo
	progress(fls[0], adapter, backupPath)

	return fileCount, nil
}

func progress(pth, adapter, backupPath string) {
	logrus.Debug("progress")
	defer del(pth)

	fileMu.Lock()
	defer fileMu.Unlock()
	fl, err := os.Open(pth)
	if err != nil {
		logrus.Error(err)
		return
	}
	defer fl.Close()

	jobs := make(chan Job)
	results := make(chan Result)
	massages := make(chan Massage)
	wg := new(sync.WaitGroup)

	for i := 0; i < setting.ParseGoroutineCount; i++ {
		wg.Add(1)
		logrus.Debug(i)
		go processJob(jobs, results, wg, adapter)
	}

	go send(massages)

	go func() {
		logrus.Debug("add jobs")
		sc := bufio.NewScanner(fl)
		for sc.Scan() {
			jobs <- Job{
				content: sc.Text(),
			}
		}
		close(jobs)
	}()

	go func() {
		wg.Wait()
		close(results)
	}()

	// one and one go
	for v := range results {
		suffix := ".log"
		allRowCount++
		switch v.end {
		case ResultSuccess:
			successCount++
			massages <- Massage{
				topic: v.topic,
				msg:   v.csv,
			}
			continue
		case ResultFailedParse:
			failedParseCount++
			suffix = ".parse"
		case ResultFailedValidate:
			failedValidateCount++
			suffix = ".validate"
		case ResultFailedCsv:
			failedCsvCount++
			suffix = ".csv"
		}
		logrus.Debugf("Result %v", v)
		// 每小时一个备份
		fileName := time.Now().Format("2006-01-02-15") + suffix
		p := path.Join(backupPath, fileName)
		writeFile(p, v.source)
	}

	close(massages)
	logrus.Debug("close massages")
}

func processJob(jobs <-chan Job, results chan<- Result, wg *sync.WaitGroup, adapter string) {
	defer wg.Done()
	for j := range jobs {
		var csv string
		var err error
		logrus.Debugf("Process job: %v", j)
		fn := adapters[adapter]
		model := fn()
		tp := topics[adapter]

		if err = json.Unmarshal([]byte(j.content), &model); err != nil {
			logrus.Errorf("Parse content %s error: %v", j.content, err)
			results <- Result{
				source: j.content,
				csv:    "",
				topic:  tp,
				end:    ResultFailedParse,
			}
			continue
		}
		if err = model.Validate(); err != nil {
			logrus.Errorf("Validate content %s error: %v", j.content, err)
			results <- Result{
				source: j.content,
				csv:    "",
				topic:  tp,
				end:    ResultFailedValidate,
			}
			continue
		}

		if csv, err = model.Csv(); err != nil {
			logrus.Errorf("Csv error: %v", err)
			results <- Result{
				source: j.content,
				csv:    "",
				topic:  tp,
				end:    ResultFailedCsv,
			}
			continue
		}

		results <- Result{
			source: j.content,
			csv:    strings.TrimSpace(csv),
			topic:  tp,
			end:    ResultSuccess,
		}
		logrus.Debug("Finish job")
	}
}

// 只有单个写文件，不锁
func writeFile(path, content string) {
	file, err := os.OpenFile(path, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0660)
	if err != nil {
		logrus.Errorf("Write file error: %v", err)
		return
	}
	defer file.Close()

	_, err = file.WriteString(content + "\n")
	if err != nil {
		logrus.Errorf("Write string error: %v", err)
		return
	}
	logrus.Debugf("Write file %s success", path)
}

func del(p string) {
	delMu.Lock()
	defer delMu.Unlock()

	if helper.IsFile(p) {
		if err := os.Remove(p); err != nil {
			logrus.Errorf("Remove file %s error: %v", p, err)
			return
		}
		logrus.Debugf("Remove %s success", p)
	}
}

func register(name string, m Modeler) {
	adapters[name] = m
}

func send(msgs <-chan Massage) {
	logrus.Debug("Send massage")
	count := 0
	batch := setting.SendBatchCount
	ms := make([]*sarama.ProducerMessage, 0, batch)
	for v := range msgs {
		count++
		logrus.Debugf("Send [%v]", v)
		m := kafka.NewMassage(v.topic, v.msg)
		ms = append(ms, m)
		if count == batch {
			count = 0
			if err := kafka.SendMsgs(ms); err != nil {
				logrus.Error(err)
			}
			ms = make([]*sarama.ProducerMessage, 0, batch)
			continue
		}
	}

	if err := kafka.SendMsgs(ms); err != nil {
		logrus.Error(err)
	}
	logrus.Debug("End send")
}
