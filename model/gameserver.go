package model

import "fmt"

// Gameserver 游戏服信息
type Gameserver struct {
	GameserverID uint `json:"gameserver_id" gorm:"column:gameserver_id;primary_key"`
	ProductID    uint `json:"product_id" gorm:"column:product_id"`
	PlatformID   uint `json:"platform_id" gorm:"column:platform_id"`
	No           uint `json:"no" gorm:"column:no"`
}

// TableName 指定表名
func (Gameserver) TableName() string {
	return "gameservers"
}

// GetGameserver 获取 pid plid no 的游戏服
func GetGameserver(pid, plid, no uint) (*Gameserver, error) {
	m := newGameserver(pid, plid, no)
	return m, GetModel(m)
}

func newGameserver(pid, plid, no uint) *Gameserver {
	return &Gameserver{
		ProductID:  pid,
		PlatformID: plid,
		No:         no,
	}
}

func (m *Gameserver) key() string {
	return fmt.Sprintf("gameserver-%d-%d-%d", m.ProductID, m.PlatformID, m.No)
}

func (m *Gameserver) Isset() bool {
	return m.GameserverID != 0
}

func (m *Gameserver) dbGet() {
	DB.Where(&Gameserver{
		ProductID:  m.ProductID,
		PlatformID: m.PlatformID,
		No:         m.No,
	}).First(&m)
}

func (m *Gameserver) remoteCreate() error {
	return nil
}

func (m *Gameserver) errors() error {
	return fmt.Errorf("gameserver not found product_id: %d, platform_id: %d, no: %d", m.ProductID, m.PlatformID, m.No)
}
