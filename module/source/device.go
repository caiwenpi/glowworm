package source

import (
	"glowworm/model"
	"glowworm/module/csvs"

	"bytes"

	"gopkg.in/asaskevich/govalidator.v4"
)

// Device 设备日志信息
type Device struct {
	DwDevice

	ProductID    int32
	ProductName  string `json:"product_name" valid:"required,length(0|32)"`
	PlatformID   int32
	PlatformName string `json:"platform_name" valid:"required,length(0|32)"`
	ChannelID    int32
	ChannelName  string `json:"channel_name" valid:"required,length(0|32)"`

	DwIPTime
}

func NewDevice() ModelInterface {
	return new(Device)
}

// Validate 验证数据
func (m *Device) Validate() error {
	var err error
	var product *model.Product
	var platform *model.Platform
	var channel *model.Channel

	_, err = govalidator.ValidateStruct(m)
	if err != nil {
		return err
	}
	product, err = model.GetProduct(m.ProductName)
	if err != nil {
		return err
	}
	m.ProductID = int32(product.ProductID)

	platform, err = model.GetPlatform(product.ProductID, m.PlatformName)
	if err != nil {
		return err
	}
	m.PlatformID = int32(platform.PlatformID)

	channel, err = model.GetChannel(product.ProductID, m.ChannelName)
	if err != nil {
		return err
	}
	m.ChannelID = int32(channel.ChannelID)

	err = m.processDevice()
	if err != nil {
		return err
	}

	err = m.processIPTime()
	if err != nil {
		return err
	}

	return nil
}

// Csv 准备上传数据
func (m *Device) Csv() (string, error) {
	var s string
	msg := bytes.NewBufferString(s)
	sl := []*Device{}
	sl = append(sl, m)
	if err := csvs.MarshalWithoutHeaders(&sl, msg); err != nil {
		return "", err
	}
	return msg.String(), nil
}

func init() {
	register("device", NewDevice)
}
