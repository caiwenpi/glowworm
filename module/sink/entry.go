package sink

import (
	"glowworm/module/impala"
)

type Entry struct {
	Modeler
}

func newEntry() *Entry {
	m := new(Entry)
	m.dir = "entry"
	m.table = "tmp_raw_entry"
	return m
}

func (m *Entry) Load() error {
	var err error
	query := `
		INSERT INTO raw_entry PARTITION (pid, year)
		SELECT
			t.*,
			CAST(t.product_id AS TINYINT),
			CAST(FROM_UNIXTIME(t.time, 'yy') AS TINYINT)
		FROM tmp_raw_entry AS t
		LEFT JOIN raw_entry AS l ON (
			t.product_id = l.product_id
			AND t.unique_account_id = l.unique_account_id
			AND t.step = l.step
		)
		WHERE l.unique_account_id IS NULL
	`

	err = impala.Query(query)
	if err != nil {
		return err
	}

	err = impala.Compute("raw_entry")
	if err != nil {
		return err
	}

	err = impala.Truncate(m.Table())
	if err != nil {
		return err
	}

	return nil
}

func init() {
	register("entry", newEntry())
}
