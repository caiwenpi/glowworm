package source

import (
	"bytes"
	"glowworm/model"
	"glowworm/module/csvs"

	"gopkg.in/asaskevich/govalidator.v4"
)

// VirtualCurrency 虚拟币日志信息
type VirtualCurrency struct {
	DwProduct
	DwIPTime
	DwAccount

	CategoryID     int32
	Category       string `json:"category" valid:"required,length(0|64)"`
	ProjectID      int32
	Project        string `json:"project" valid:"required,length(0|64)"`
	Info           string `json:"info"`
	OpType         int32
	OpCount        int32 `json:"op_count" valid:"required"`
	QuantityBefore int32 `json:"quantity_before"`
	Quantity       int32
}

func NewVirtualCurrency() ModelInterface {
	return new(VirtualCurrency)
}

// Validate 验证数据
func (m *VirtualCurrency) Validate() error {
	var err error
	_, err = govalidator.ValidateStruct(m)
	if err != nil {
		return err
	}

	err = m.processProduct()
	if err != nil {
		return err
	}
	err = m.processIPTime()
	if err != nil {
		return err
	}
	err = m.processAccount()
	if err != nil {
		return err
	}
	m.UniqueChrID = SetUniqueChrID(m.ProductID, m.PlatformID, m.GameserverID, m.ChrID)

	var vc *model.VirtualCurrency
	vc, err = model.GetVirtualCurrency(uint(m.ProductID), m.Category)
	if err != nil {
		return err
	}
	m.CategoryID = int32(vc.VCID)

	var vp *model.VirtualCurrencyProject
	vp, err = model.GetVirtualCurrencyProject(uint(m.ProductID), m.Project)
	if err != nil {
		return err
	}
	m.ProjectID = int32(vp.VPID)

	if m.OpCount > 0 {
		m.OpType = opTypeAdd
	}
	m.Quantity = m.QuantityBefore + m.OpCount

	return nil
}

// Csv 准备上传数据
func (m *VirtualCurrency) Csv() (string, error) {
	var s string
	msg := bytes.NewBufferString(s)
	sl := []*VirtualCurrency{}
	sl = append(sl, m)
	if err := csvs.MarshalWithoutHeaders(&sl, msg); err != nil {
		return "", err
	}
	return msg.String(), nil
}

func init() {
	register("virtual-currency", NewVirtualCurrency)
}
