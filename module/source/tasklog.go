package source

import (
	"bytes"
	"glowworm/model"
	"glowworm/module/csvs"

	"fmt"
	"glowworm/module/helper"

	"gopkg.in/asaskevich/govalidator.v4"
)

// TaskLog 任务日志信息
type TaskLog struct {
	DwProduct
	DwIPTime
	DwAccount

	Operate      int32 `json:"operate"` // 0，1，2
	UniqueTaskID int32
	TaskID       int32  `json:"task_id" valid:"required"`
	Name         string `json:"name" valid:"required,length(0|32)"`
	UniqueTypeID int32
	TypeID       int32  `json:"type_id" valid:"required"`
	Type         string `json:"type" valid:"required,length(0|32)"`
	Result       int32  `json:"result"` // 0, 1
	Reason       string `json:"reason" valid:"-,length(0|255)"`
	SessionID    string `json:"session_id" valid:"required,length(0|32)"`
}

func NewTaskLog() ModelInterface {
	return new(TaskLog)
}

// Validate 验证数据
func (m *TaskLog) Validate() error {
	var err error
	_, err = govalidator.ValidateStruct(m)
	if err != nil {
		return err
	}

	if !helper.ContainsInt32([]int32{0, 1, 2}, m.Operate) {
		return fmt.Errorf("operate must in (0,1,2)")
	}

	if !helper.ContainsInt32([]int32{0, 1}, m.Result) {
		return fmt.Errorf("result must in (0,1)")
	}

	err = m.processProduct()
	if err != nil {
		return err
	}
	err = m.processIPTime()
	if err != nil {
		return err
	}
	err = m.processAccount()
	if err != nil {
		return err
	}
	m.UniqueChrID = SetUniqueChrID(m.ProductID, m.PlatformID, m.GameserverID, m.ChrID)

	var ty *model.TaskType
	ty, err = model.GetTaskType(uint(m.ProductID), m.TypeID, m.Type)
	if err != nil {
		return err
	}
	m.UniqueTypeID = int32(ty.UniqueTypeID)

	var t *model.Task
	t, err = model.GetTask(uint(m.ProductID), m.TaskID, m.Name)
	if err != nil {
		return err
	}
	m.UniqueTaskID = int32(t.UniqueTaskID)

	return nil
}

// Csv 准备上传数据
func (m *TaskLog) Csv() (string, error) {
	var s string
	msg := bytes.NewBufferString(s)
	sl := []*TaskLog{}
	sl = append(sl, m)
	if err := csvs.MarshalWithoutHeaders(&sl, msg); err != nil {
		return "", err
	}
	return msg.String(), nil
}

func init() {
	register("task-log", NewTaskLog)
}
