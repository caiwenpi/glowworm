package model

import (
	"encoding/json"
	"fmt"
	"glowworm/module/httpapi"
	"net/url"
	"sync"

	"github.com/Sirupsen/logrus"
)

var VirtualItemSourceMu = new(sync.Mutex)

// VirtualItemSource 虚拟物品来源
type VirtualItemSource struct {
	ISID      uint   `json:"is_id" gorm:"column:is_id;primary_key"`
	ProductID uint   `json:"product_id" gorm:"column:product_id"`
	Name      string `json:"name" gorm:"column:name"`
}

// TableName 表名
func (VirtualItemSource) TableName() string {
	return "dw_virtual_item_source"
}

// GetVirtualItemSource 获取
func GetVirtualItemSource(pid uint, name string) (*VirtualItemSource, error) {
	m := newVirtualItemSource(pid, name)
	return m, GetModel(m)
}

func newVirtualItemSource(pid uint, name string) *VirtualItemSource {
	return &VirtualItemSource{
		ProductID: pid,
		Name:      name,
	}
}

func (m *VirtualItemSource) key() string {
	return fmt.Sprintf("virtual-item-source-%d-%s", m.ProductID, m.Name)
}

func (m *VirtualItemSource) Isset() bool {
	return m.ISID != 0
}

func (m *VirtualItemSource) dbGet() {
	DB.Where(&VirtualItemSource{
		ProductID: m.ProductID,
		Name:      m.Name,
	}).First(&m)
}

func (m *VirtualItemSource) remoteCreate() error {
	VirtualItemSourceMu.Lock()
	defer VirtualItemSourceMu.Unlock()

	body, err := httpapi.PostForm("virtual-item-source", url.Values{
		"product_id": {fmt.Sprint(m.ProductID)},
		"name":       {m.Name},
	})

	if err != nil {
		return err
	}

	tmp := new(VirtualItemSource)
	if err = json.Unmarshal(body, &tmp); err != nil {
		logrus.Warn(err)
	}
	m.ISID = tmp.ISID
	return nil
}

func (m *VirtualItemSource) errors() error {
	return fmt.Errorf("virtual item source not found product_id: %d, name: %s", m.ProductID, m.Name)
}
