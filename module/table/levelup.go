package table

import (
	"fmt"
	"glowworm/module/impala"
	"strings"

	"github.com/Sirupsen/logrus"
)

func levelup() {
	var err error
	table := "level_up"
	sl := []string{
		column(clmProduct),
		column(clmIPTime),
		column(clmAccount),
		`
		currently_exp BIGINT,
		exp BIGINT,
		time_span INT
		`,
	}

	clms := strings.Join(sl, ",")

	raw := fmt.Sprintf(template(tmplRaw), table, clms, partition(pttPYM))

	err = impala.Query(raw)
	if err != nil {
		logrus.Error(err)
	}

	sl = []string{
		column(clmProduct),
		column(clmIPTime),
		column(clmAccount),
		`
		currently_exp BIGINT,
		exp BIGINT
		`,
	}

	clms = strings.Join(sl, ",")

	tmp := fmt.Sprintf(template(tmplTmp), table, clms, table)
	err = impala.Query(tmp)
	if err != nil {
		logrus.Error(err)
	}
}
