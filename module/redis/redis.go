package redis

import (
	"time"

	redisbase "github.com/go-redis/redis"
)

var (
	Client *redisbase.Client

	duration time.Duration = 7 * 24 * 3600 * time.Second
)

func Set(key string, value interface{}) error {
	return Client.Set(key, value, duration).Err()
}

func Get(key string) (string, error) {
	val, err := Client.Get(key).Result()
	if err == redisbase.Nil {
		return "", nil
	} else if err != nil {
		return "", err
	}
	return val, nil
}
