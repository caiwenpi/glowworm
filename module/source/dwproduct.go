package source

import (
	"glowworm/model"

	"github.com/asaskevich/govalidator"
)

// DwProduct 封装游戏信息
type DwProduct struct {
	ProductID    int32
	ProductName  string `json:"product_name" valid:"required,length(0|32)"`
	PlatformID   int32
	PlatformName string `json:"platform_name" valid:"required,length(0|32)"`
	ChannelID    int32
	ChannelName  string `json:"channel_name" valid:"required,length(0|32)"`
	GameserverID int32
	GameserverNo int32 `json:"gameserver_no" valid:"required"`
}

func (m *DwProduct) processProduct() error {
	var err error
	var product *model.Product
	var platform *model.Platform
	var channel *model.Channel
	var gameserver *model.Gameserver

	_, err = govalidator.ValidateStruct(m)
	if err != nil {
		return err
	}
	product, err = model.GetProduct(m.ProductName)
	if err != nil {
		return err
	}
	m.ProductID = int32(product.ProductID)

	platform, err = model.GetPlatform(product.ProductID, m.PlatformName)
	if err != nil {
		return err
	}
	m.PlatformID = int32(platform.PlatformID)

	channel, err = model.GetChannel(product.ProductID, m.ChannelName)
	if err != nil {
		return err
	}
	m.ChannelID = int32(channel.ChannelID)

	gameserver, err = model.GetGameserver(product.ProductID, platform.PlatformID, uint(m.GameserverNo))
	if err != nil {
		return err
	}
	m.GameserverID = int32(gameserver.GameserverID)
	return nil
}
