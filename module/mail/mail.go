package mail

import (
	"bytes"
	"encoding/json"
	"fmt"

	gomail "gopkg.in/gomail.v2"

	"github.com/Sirupsen/logrus"
)

const (
	format = "2006-01-02 15:04:05"
)

// MailHook send mail
type MailHook struct {
	Der     *gomail.Dialer
	From    string
	To      string
	Subject string
}

// NewHook send mail
func NewHook(subject, host string, port int, from, to, user, password string) (*MailHook, error) {
	d := gomail.NewDialer(host, port, user, password)
	s, err := d.Dial()
	if err != nil {
		return nil, err
	}
	defer s.Close()
	return &MailHook{
		Der:     d,
		From:    from,
		To:      to,
		Subject: subject,
	}, nil
}

// Fire is called when a log event is fired.
func (hook *MailHook) Fire(entry *logrus.Entry) error {
	message := createMessage(entry)

	m := gomail.NewMessage()
	m.SetHeader("From", hook.From)
	m.SetHeader("To", hook.To)
	m.SetHeader("Subject", hook.Subject)
	m.SetBody("text/html", message.String())

	go func() {
		hook.Der.DialAndSend(m)
	}()

	return nil
}

// Levels returns the available logging levels.
func (hook *MailHook) Levels() []logrus.Level {
	return []logrus.Level{
		logrus.PanicLevel,
		logrus.FatalLevel,
		logrus.ErrorLevel,
	}
}

func createMessage(entry *logrus.Entry) *bytes.Buffer {
	body := entry.Level.String() + "-" + entry.Time.Format(format) + " - " + entry.Message
	fields, _ := json.MarshalIndent(entry.Data, "", "\t")
	contents := fmt.Sprintf("%s\r\n\r\n%s", body, fields)
	message := bytes.NewBufferString(contents)
	return message
}
