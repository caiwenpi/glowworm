package source

import (
	"bytes"
	"glowworm/module/csvs"

	"golang.org/x/text/currency"

	"gopkg.in/asaskevich/govalidator.v4"
)

// Payment 充值日志信息
type Payment struct {
	DwProduct
	DwIPTime
	DwAccount
	DwDevice

	OrderID        string  `json:"order_id" valid:"required,length(0|120)"`
	CurrencyType   string  `json:"currency_type" valid:"required"`
	CurrencyAmount float32 `json:"currency_amount" valid:"required"`
	PayCategory    int32   `json:"pay_category" valid:"required"`
	PackageName    string  `json:"package_name" valid:"-,length(0|64)"`
	PayChannel     string  `json:"pay_channel" valid:"-,length(0|64)"`
	VirtualAmount  int32   `json:"virtual_amount" valid:"required"`
}

func NewPayment() ModelInterface {
	return new(Payment)
}

// Validate 验证数据
func (m *Payment) Validate() error {
	var err error
	_, err = govalidator.ValidateStruct(m)
	if err != nil {
		return err
	}

	err = m.processProduct()
	if err != nil {
		return err
	}
	err = m.processIPTime()
	if err != nil {
		return err
	}
	err = m.processAccount()
	if err != nil {
		return err
	}
	m.UniqueChrID = SetUniqueChrID(m.ProductID, m.PlatformID, m.GameserverID, m.ChrID)

	err = m.processDevice()
	if err != nil {
		return err
	}

	_, err = currency.ParseISO(m.CurrencyType)
	if err != nil {
		return err
	}

	return nil
}

// Csv 准备上传数据
func (m *Payment) Csv() (string, error) {
	var s string
	msg := bytes.NewBufferString(s)
	sl := []*Payment{}
	sl = append(sl, m)
	if err := csvs.MarshalWithoutHeaders(&sl, msg); err != nil {
		return "", err
	}
	return msg.String(), nil
}

func init() {
	register("payment", NewPayment)
}
