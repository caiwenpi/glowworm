package sink

import (
	"glowworm/module/impala"
)

type VirtualItem struct {
	Modeler
}

func newVirtualItem() *VirtualItem {
	m := new(VirtualItem)
	m.dir = "virtual-items"
	m.table = "tmp_raw_virtual_items"
	return m
}

func (m *VirtualItem) Load() error {
	var err error
	query := `
		INSERT INTO raw_virtual_items PARTITION (pid, year, month)
		SELECT
			t.*,
			CAST(t.product_id AS TINYINT),
			CAST(FROM_UNIXTIME(t.time, 'yy') AS TINYINT),
			CAST(FROM_UNIXTIME(t.time, 'MM') AS TINYINT)
		FROM tmp_raw_virtual_items AS t
	`

	err = impala.Query(query)
	if err != nil {
		return err
	}

	err = impala.Compute("raw_virtual_items")
	if err != nil {
		return err
	}

	err = impala.Truncate(m.Table())
	if err != nil {
		return err
	}

	return nil
}

func init() {
	register("virtual-item", newVirtualItem())
}
