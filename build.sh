#!/bin/sh
echo "building ..."
rm -rf ./glowworm-linux
mkdir ./glowworm-linux
GOOS=linux GOARCH=amd64 go build -o ./glowworm-linux/glowworm
cp -r ./conf ./glowworm-linux/
tar zcvf ./glowworm-linux.tar.gz ./glowworm-linux
echo "done."