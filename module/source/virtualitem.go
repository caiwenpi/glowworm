package source

import (
	"bytes"
	"glowworm/model"
	"glowworm/module/csvs"

	"gopkg.in/asaskevich/govalidator.v4"
)

const (
	opTypeAdd int32 = 1
	opTypeSub int32 = 0
)

// VirtualItem 虚拟物品日志信息
type VirtualItem struct {
	DwProduct
	DwIPTime
	DwAccount

	ItemUniqueID int32
	ItemID       int32  `json:"item_id" valid:"required"`
	ItemName     string `json:"item_name" valid:"required,length(0|64)"`
	TypeID       int32
	TypeName     string `json:"type_name" valid:"required,length(0|64)"`
	OpType       int32
	OpCount      int32 `json:"op_count" valid:"required"`
}

func NewVirtualItem() ModelInterface {
	return new(VirtualItem)
}

// Validate 验证数据
func (m *VirtualItem) Validate() error {
	var err error
	_, err = govalidator.ValidateStruct(m)
	if err != nil {
		return err
	}

	err = m.processProduct()
	if err != nil {
		return err
	}
	err = m.processIPTime()
	if err != nil {
		return err
	}
	err = m.processAccount()
	if err != nil {
		return err
	}
	m.UniqueChrID = SetUniqueChrID(m.ProductID, m.PlatformID, m.GameserverID, m.ChrID)

	var vi *model.VirtualItem
	vi, err = model.GetVirtualItem(uint(m.ProductID), m.ItemID, m.ItemName)
	if err != nil {
		return err
	}
	m.ItemUniqueID = int32(vi.UniqueItemID)

	var vs *model.VirtualItemSource
	vs, err = model.GetVirtualItemSource(uint(m.ProductID), m.TypeName)
	if err != nil {
		return err
	}
	m.TypeID = int32(vs.ISID)

	if m.OpCount > 0 {
		m.OpType = opTypeAdd
	}

	return nil
}

// Csv 准备上传数据
func (m *VirtualItem) Csv() (string, error) {
	var s string
	msg := bytes.NewBufferString(s)
	sl := []*VirtualItem{}
	sl = append(sl, m)
	if err := csvs.MarshalWithoutHeaders(&sl, msg); err != nil {
		return "", err
	}
	return msg.String(), nil
}

func init() {
	register("virtual-item", NewVirtualItem)
}
