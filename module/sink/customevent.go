package sink

import (
	"glowworm/module/impala"
)

type CustomEvent struct {
	Modeler
}

func newCustomEvent() *CustomEvent {
	m := new(CustomEvent)
	m.dir = "custom-event"
	m.table = "tmp_raw_custom_event"
	return m
}

func (m *CustomEvent) Load() error {
	var err error
	query := `
		INSERT INTO raw_custom_event PARTITION (pid, year, month, eid)
		SELECT
			t.*,
			CAST(t.product_id AS TINYINT),
			CAST(FROM_UNIXTIME(t.time, 'yy') AS TINYINT),
			CAST(FROM_UNIXTIME(t.time, 'MM') AS TINYINT),
			CAST(t.event_id AS INT)
		FROM tmp_raw_custom_event AS t
	`

	err = impala.Query(query)
	if err != nil {
		return err
	}

	err = impala.Compute("raw_custom_event")
	if err != nil {
		return err
	}

	err = impala.Truncate(m.Table())
	if err != nil {
		return err
	}

	return nil
}

func init() {
	register("custom-event", newCustomEvent())
}
