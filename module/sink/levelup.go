package sink

import (
	"glowworm/module/impala"
)

type LevelUp struct {
	Modeler
}

func newLevelUp() *LevelUp {
	m := new(LevelUp)
	m.dir = "level-up"
	m.table = "tmp_raw_level_up"
	return m
}

func (m *LevelUp) Load() error {
	var err error
	query := `
		INSERT INTO raw_level_up PARTITION (pid, year, month)
		SELECT
			t.product_id,
			t.product_name,
			t.platform_id,
			t.platform_name,
			t.channel_id,
			t.channel_name,
			t.gameserver_id,
			t.gameserver_no,
			t.ip,
			t.region,
			t.the_date,
			t.the_date_time,
			t.hour,
			t.time,
			t.account_id,
			t.account_name,
			t.unique_chr_id,
			t.chr_id,
			t.chr_name,
			t.chr_lvl,
			t.chr_lvl_vip,
			t.career,
			t.gender,

			t.currently_exp,
			t.exp,
			CAST(t.time - l.time AS INT) AS time_span,

			CAST(t.product_id AS TINYINT),
			CAST(FROM_UNIXTIME(t.time, 'yy') AS TINYINT),
			CAST(FROM_UNIXTIME(t.time, 'MM') AS TINYINT)
		FROM tmp_raw_level_up AS t
		LEFT JOIN raw_level_up AS l ON (
			t.product_id = l.product_id
			AND t.unique_chr_id = l.unique_chr_id
			AND t.chr_lvl = l.chr_lvl + 1
		)
		LEFT JOIN raw_level_up AS u ON (
			t.product_id = u.product_id
			AND t.unique_chr_id = u.unique_chr_id
			AND t.chr_lvl = u.chr_lvl
		)
		WHERE u.unique_chr_id IS NULL
	`

	err = impala.Query(query)
	if err != nil {
		return err
	}

	err = impala.Compute("raw_level_up")
	if err != nil {
		return err
	}

	err = impala.Truncate(m.Table())
	if err != nil {
		return err
	}

	return nil
}

func init() {
	register("level-up", newLevelUp())
}
