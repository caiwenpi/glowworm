package sink

import (
	"glowworm/module/impala"
)

type VirtualCurrency struct {
	Modeler
}

func newVirtualCurrency() *VirtualCurrency {
	m := new(VirtualCurrency)
	m.dir = "virtual-currencies"
	m.table = "tmp_raw_virtual_currencies"
	return m
}

func (m *VirtualCurrency) Load() error {
	var err error
	query := `
		INSERT INTO raw_virtual_currencies PARTITION (pid, year, month)
		SELECT
			t.*,
			CAST(t.product_id AS TINYINT),
			CAST(FROM_UNIXTIME(t.time, 'yy') AS TINYINT),
			CAST(FROM_UNIXTIME(t.time, 'MM') AS TINYINT)
		FROM tmp_raw_virtual_currencies AS t
	`

	err = impala.Query(query)
	if err != nil {
		return err
	}

	err = impala.Compute("raw_virtual_currencies")
	if err != nil {
		return err
	}

	err = impala.Truncate(m.Table())
	if err != nil {
		return err
	}

	return nil
}

func init() {
	register("virtual-currency", newVirtualCurrency())
}
