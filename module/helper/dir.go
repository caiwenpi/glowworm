package helper

import (
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

func WorkDir() (string, error) {
	p, err := ExecPath()
	if err != nil {
		return "", err
	}

	i := strings.LastIndex(p, "/")

	if i == -1 {
		return p, nil
	}
	return p[:i], nil
}

func ExecPath() (string, error) {
	f, err := exec.LookPath(os.Args[0])
	if err != nil {
		return "", err
	}

	return filepath.Abs(f)
}

func IsDir(dir string) bool {
	f, err := os.Stat(dir)
	if err != nil {
		return false
	}
	return f.IsDir()
}
