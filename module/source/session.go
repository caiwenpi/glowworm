package source

import (
	"bytes"
	"glowworm/module/csvs"

	"gopkg.in/asaskevich/govalidator.v4"
)

// Session 登出日志信息
type Session struct {
	DwProduct
	DwIPTime
	DwAccount

	SessionID string `json:"session_id" valid:"required,length(0|32)"`
}

func NewSession() ModelInterface {
	return new(Session)
}

// Validate 验证数据
func (m *Session) Validate() error {
	var err error
	_, err = govalidator.ValidateStruct(m)
	if err != nil {
		return err
	}

	err = m.processProduct()
	if err != nil {
		return err
	}
	err = m.processIPTime()
	if err != nil {
		return err
	}
	err = m.processAccount()
	if err != nil {
		return err
	}
	m.UniqueChrID = SetUniqueChrID(m.ProductID, m.PlatformID, m.GameserverID, m.ChrID)

	return nil
}

// Csv 准备上传数据
func (m *Session) Csv() (string, error) {
	var s string
	msg := bytes.NewBufferString(s)
	sl := []*Session{}
	sl = append(sl, m)
	if err := csvs.MarshalWithoutHeaders(&sl, msg); err != nil {
		return "", err
	}
	return msg.String(), nil
}

func init() {
	register("session", NewSession)
}
