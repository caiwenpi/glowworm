package helper

import (
	"fmt"
	"os"
	"path"
	"sort"
)

func GetFiles(dir string) ([]os.FileInfo, error) {
	if !IsDir(dir) {
		return nil, fmt.Errorf("path is not a dir: %s", dir)
	}

	f, err := os.Open(dir)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	fls, err := f.Readdir(0)
	if err != nil {
		return nil, err
	}
	return fls, nil
}

func GetFilesPathSortByMTime(dir string) ([]string, error) {
	fls, err := GetFiles(dir)
	if err != nil {
		return nil, err
	}

	sort.Slice(fls, func(i, j int) bool {
		return fls[i].ModTime().Before(fls[j].ModTime())
	})

	sl := make([]string, 0, len(fls))
	for _, f := range fls {
		sl = append(sl, path.Join(dir, f.Name()))
	}
	return sl, nil
}

func IsFile(filePath string) bool {
	f, err := os.Stat(filePath)
	if err != nil {
		return false
	}
	return !f.IsDir()
}