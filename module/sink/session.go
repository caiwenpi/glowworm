package sink

import (
	"glowworm/module/impala"
)

type Session struct {
	Modeler
}

func newSession() *Session {
	m := new(Session)
	m.dir = "session"
	m.table = "tmp_raw_sessions"
	return m
}

func (m *Session) Load() error {
	var err error
	query := `
		INSERT INTO raw_sessions PARTITION (pid, year, month)
		SELECT
			t.product_id,
			t.product_name,
			t.platform_id,
			t.platform_name,
			t.channel_id,
			t.channel_name,
			t.gameserver_id,
			t.gameserver_no,
			t.ip,
			t.region,
			t.the_date,
			t.the_date_time,
			t.hour,
			t.time,
			t.account_id,
			t.account_name,
			t.unique_chr_id,
			t.chr_id,
			t.chr_name,
			t.chr_lvl,
			t.chr_lvl_vip,
			t.career,
			t.gender,
			t.session_id,
			CAST(t.time - l.time AS INT) AS time_span,

			CAST(t.product_id AS TINYINT),
			CAST(FROM_UNIXTIME(t.time, 'yy') AS TINYINT),
			CAST(FROM_UNIXTIME(t.time, 'MM') AS TINYINT)
		FROM tmp_raw_sessions AS t
		INNER JOIN raw_logins AS l ON (
			t.product_id = l.product_id
			AND t.unique_chr_id = l.unique_chr_id
			AND t.session_id = l.session_id
			AND t.time > l.time
		)
		LEFT JOIN raw_sessions AS s ON (
			t.product_id = s.product_id
			AND t.unique_chr_id = s.unique_chr_id
			AND t.session_id = s.session_id
		)
		WHERE s.session_id IS NULL
	`

	err = impala.Query(query)
	if err != nil {
		return err
	}

	err = impala.Compute("raw_sessions")
	if err != nil {
		return err
	}

	err = impala.Truncate(m.Table())
	if err != nil {
		return err
	}

	return nil
}

func init() {
	register("session", newSession())
}
