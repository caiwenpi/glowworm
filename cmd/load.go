package cmd

import (
	"fmt"
	"time"

	"glowworm/module/helper"
	"glowworm/module/setting"
	"glowworm/module/sink"

	"github.com/Sirupsen/logrus"
	"github.com/urfave/cli"
)

// Load the hdfs data to impala
var Load = cli.Command{
	Name:   "load",
	Action: runLoad,
	Before: func(c *cli.Context) error {
		setting.NewContext(c)
		//setting.NewDBService()
		setting.NewHDFSService()
		setting.NewImpalaService()
		return nil
	},
	After: func(c *cli.Context) error {
		setting.CloseImpalaService()
		setting.CloseHDFSService()
		//setting.CloseDBService()
		return nil
	},
	Flags: []cli.Flag{
		cli.StringFlag{
			Name: "category, c",
		},
	},
}

func runLoad(c *cli.Context) error {
	var err error
	var failedCnt int
	category := c.String("category")

	if !helper.ContainsString(setting.DataCategories, category) {
		return fmt.Errorf("unknown category %s", category)
	}

	for {
		if failedCnt == setting.MaxErrCnt {
			break
		}
		logrus.Debug("Load " + category)
		if err = sink.Run(category); err != nil {
			logrus.Errorf("Sink run failed: %v", err)
			failedCnt++
		}
		time.Sleep(setting.RunInterval * time.Second)
	}
	return nil
}
