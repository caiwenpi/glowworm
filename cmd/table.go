package cmd

import (
	"glowworm/module/helper"
	"glowworm/module/setting"
	"glowworm/module/table"

	"errors"

	"github.com/Sirupsen/logrus"
	"github.com/urfave/cli"
)

var Table = cli.Command{
	Name:   "table",
	Action: runTable,
	Before: func(c *cli.Context) error {
		setting.NewContext(c)
		setting.NewImpalaService()
		return nil
	},
	After: func(c *cli.Context) error {
		setting.CloseImpalaService()
		return nil
	},
	Flags: []cli.Flag{
		cli.StringFlag{
			Name: "category, c",
		},
		cli.StringFlag{
			Name: "password, p",
		},
	},
}

func runTable(c *cli.Context) error {
	logrus.Info("Running table")
	category := c.String("category")
	password := c.String("password")

	if helper.ContainsString([]string{"truncate", "drop"}, category) {
		if password != setting.TablePassword {
			return errors.New("Who are you?")
		}
	}

	return table.Run(category)
}
