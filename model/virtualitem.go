package model

import (
	"encoding/json"
	"fmt"
	"glowworm/module/httpapi"
	"net/url"
	"sync"

	"github.com/Sirupsen/logrus"
)

var VirtualItemMu = new(sync.Mutex)

// VirtualItem 任务
type VirtualItem struct {
	UniqueItemID uint   `json:"unique_item_id" gorm:"column:unique_item_id;primary_key"`
	ProductID    uint   `json:"product_id" gorm:"column:product_id"`
	ItemID       int32  `json:"item_id" gorm:"column:item_id"`
	Name         string `json:"name" gorm:"column:name"`
}

// TableName 表名
func (VirtualItem) TableName() string {
	return "dw_virtual_items"
}

// GetVirtualItem 获取
func GetVirtualItem(pid uint, itemID int32, name string) (*VirtualItem, error) {
	m := newVirtualItem(pid, itemID, name)
	return m, GetModel(m)
}

func newVirtualItem(pid uint, itemID int32, name string) *VirtualItem {
	return &VirtualItem{
		ProductID: pid,
		ItemID:    itemID,
		Name:      name,
	}
}

func (m *VirtualItem) key() string {
	return fmt.Sprintf("virtual-item-%d-%d-%s", m.ProductID, m.ItemID, m.Name)
}

func (m *VirtualItem) Isset() bool {
	return m.UniqueItemID != 0
}

func (m *VirtualItem) dbGet() {
	DB.Where(&VirtualItem{
		ProductID: m.ProductID,
		ItemID:    m.ItemID,
		Name:      m.Name,
	}).First(&m)
}

func (m *VirtualItem) remoteCreate() error {
	VirtualItemMu.Lock()
	defer VirtualItemMu.Unlock()

	body, err := httpapi.PostForm("virtual-item", url.Values{
		"product_id": {fmt.Sprint(m.ProductID)},
		"item_id":    {fmt.Sprint(m.ItemID)},
		"name":       {m.Name},
	})
	if err != nil {
		return err
	}

	tmp := new(VirtualItem)
	if err = json.Unmarshal(body, &tmp); err != nil {
		logrus.Warn(err)
	}
	m.UniqueItemID = tmp.UniqueItemID
	return nil
}

func (m *VirtualItem) errors() error {
	return fmt.Errorf("virtual item not found product_id: %d, item_id: %d, name: %s", m.ProductID, m.ItemID, m.Name)
}
