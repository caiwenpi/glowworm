package source

import (
	"bytes"
	"glowworm/module/csvs"
	"glowworm/module/setting"
	"time"

	"gopkg.in/asaskevich/govalidator.v4"
)

// Online 在线日志信息
type Online struct {
	DwProduct

	TheDate  string
	DateTime string `json:"date_time" valid:"required"`
	Hour     int
	Time     int64

	Num int32 `json:"num" valid:"required"`
}

func NewOnline() ModelInterface {
	return new(Online)
}

// Validate 验证数据
func (m *Online) Validate() error {
	var err error
	_, err = govalidator.ValidateStruct(m)
	if err != nil {
		return err
	}

	err = m.processProduct()
	if err != nil {
		return err
	}

	loc, err := time.LoadLocation("PRC")
	if err != nil {
		return err
	}

	t, err := time.ParseInLocation(setting.TimestampFormat, m.DateTime, loc)
	if err != nil {
		return err
	}
	m.TheDate = t.Format(setting.DateFormat)
	m.Hour = t.Hour()
	m.Time = t.Unix()

	return nil
}

// Csv 准备上传数据
func (m *Online) Csv() (string, error) {
	var s string
	msg := bytes.NewBufferString(s)
	sl := []*Online{}
	sl = append(sl, m)
	if err := csvs.MarshalWithoutHeaders(&sl, msg); err != nil {
		return "", err
	}
	return msg.String(), nil
}

func init() {
	register("online", NewOnline)
}
