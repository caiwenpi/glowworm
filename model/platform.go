package model

import "fmt"

// Platform 平台信息
type Platform struct {
	PlatformID uint   `json:"platform_id" gorm:"column:platform_id;primary_key"`
	ProductID  uint   `json:"product_id" gorm:"column:product_id"`
	Name       string `json:"name" gorm:"type:varchar(16);not null"`
}

// TableName 指定表名
func (Platform) TableName() string {
	return "platforms"
}

// GetPlatform 获取 pid name 的平台
func GetPlatform(pid uint, name string) (*Platform, error) {
	m := newPlatform(pid, name)
	return m, GetModel(m)
}

func newPlatform(pid uint, name string) *Platform {
	return &Platform{
		ProductID: pid,
		Name:      name,
	}
}

func (m *Platform) key() string {
	return fmt.Sprintf("platform-%d-%s", m.ProductID, m.Name)
}

func (m *Platform) Isset() bool {
	return m.PlatformID != 0
}

func (m *Platform) dbGet() {
	DB.Where(&Platform{
		ProductID: m.ProductID,
		Name:      m.Name,
	}).First(&m)
}

func (m *Platform) remoteCreate() error {
	return nil
}

func (m *Platform) errors() error {
	return fmt.Errorf("platform not found product_id: %d, name: %s", m.ProductID, m.Name)
}
